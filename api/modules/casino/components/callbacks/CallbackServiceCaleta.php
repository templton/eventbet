<?php

namespace app\modules\casino\components\callbacks;

use app\modules\casino\components\helpers\ServiceHelper;
use Enterra\Casino\Request\Caleta\AbstractRequest;
use Enterra\Casino\Request\RequestInterface;
use Enterra\Casino\Constants\Caleta\CaletaConstants;

/**
 * Class CallbackServiceCaleta
 *
 * @package app\modules\casino\components\callbacks
 */
class CallbackServiceCaleta extends CallbackServiceMethodAbstract
{
    public function _createRequestByMethodName(string $methodName): RequestInterface
    {
        $request =  parent::_createRequestByMethodName($methodName);

        if ($request instanceof AbstractRequest){
            $originRequest  = ServiceHelper::getRequest();

            $headers        = $originRequest->getHeaders();
            $sign           = $headers[CaletaConstants::AUTH_HEADER_NAME] ?? null;
            $requestStr     = $originRequest->getRawBody();

            $request->setRawBody($requestStr);
            $request->setSign($sign);
        }

        return $request;
    }
}