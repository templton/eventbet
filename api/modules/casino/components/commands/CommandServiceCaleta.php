<?php

namespace app\modules\casino\components\commands;

use app\common\httpClient\HttpClientTrait;
use app\models\casino\CasinoGame;
use app\models\casino\CasinoGameDevice;
use app\modules\casino\components\migrations\ProviderGamesMigration;
use app\modules\casino\components\ProviderService;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use yii\db\ActiveRecord;
use yii\httpclient\Client;
use yii\httpclient\Response;
use InvalidArgumentException;
use yii\base\InvalidConfigException;

/**
 * Class CommandServiceCaleta
 *
 * @package app\modules\casino\components\commands
 */
class CommandServiceCaleta extends CommandServiceAbstract
{
    #region use

    use HttpClientTrait;

    #endregion use

    #region constants

    const PROVIDER_FIELD_DESKTOP        = 'GPL_DESKTOP';
    const PROVIDER_FIELD_MOBILE         = 'GPL_MOBILE';

    const PROVIDER_CATEGORY_VIDEO_SLOTS = 'Video Slots';
    const PROVIDER_CATEGORY_VIDEO_BINGO = 'Video Bingo';
    const PROVIDER_CATEGORY_VIDEO_KENO  = 'Video Keno';
    const PROVIDER_CATEGORY_LOTTERY     = 'Lottery';
    const PROVIDER_CATEGORY_SCRATCH     = 'Scratch';

    const CATEGORY_MAPPING = [
        self::PROVIDER_CATEGORY_VIDEO_SLOTS => ProviderGamesMigration::CATEGORY_SLOTS,
        self::PROVIDER_CATEGORY_VIDEO_BINGO => ProviderGamesMigration::CATEGORY_LOTTERY,
        self::PROVIDER_CATEGORY_VIDEO_KENO => ProviderGamesMigration::CATEGORY_LOTTERY,
        self::PROVIDER_CATEGORY_LOTTERY => ProviderGamesMigration::CATEGORY_LOTTERY,
        self::PROVIDER_CATEGORY_SCRATCH => ProviderGamesMigration::CATEGORY_LOTTERY,
    ];

    const DATE_FORMAT = 'Y-m-d H:i:s';

    #endregion constants


    #region methods

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function _updateGames(): bool
    {
        try {
            return $this->_saveGames($this->_loadGameList());
        }catch (\Throwable $e){
            $this->_logg("ERROR: " . $e->getMessage() . '; ' . $e->getFile().'; '.$e->getLine()."\n");
        }

        return false;
    }

    /**
     * @param array $externalProviderGames
     * @return bool
     * @throws InvalidConfigException
     */
    protected function _saveGames(array $externalProviderGames): bool
    {
        $count = count($externalProviderGames);
        list(
            $countSavedTestGamesUpdated,
            $countSavedTestGamesInserted
            ) = $this->_saveGamesIteration($externalProviderGames, 1);

        list(
            $countSavedRealGamesUpdated,
            $countSavedRealGamesInserted
            ) = $this->_saveGamesIteration($externalProviderGames, 0);

        $allSum = $countSavedTestGamesUpdated +
            $countSavedTestGamesInserted +
            $countSavedRealGamesInserted +
            $countSavedRealGamesUpdated;

        $allProcessed = (2 * $count) === $allSum;

        $message = "All income games: $count. \n";
        $message .= "TEST: Insert=$countSavedTestGamesInserted, Update=$countSavedTestGamesUpdated\n";
        $message .= "Real: Insert=$countSavedRealGamesInserted, Update=$countSavedRealGamesUpdated\n";
        $message .= $allProcessed
            ? "All income games success processed"
            : "Error: some games are not processed";

        $this->_logg($message);

        return true;
    }

    /**
     * @param array $externalProviderGames
     * @param $isTest
     * @return int[]
     * @throws InvalidConfigException
     */
    protected function _saveGamesIteration(array $externalProviderGames, $isTest): array
    {
        $countUpdated=0;
        $countInserted=0;

        $internalGames = $this->_getInternalGames($isTest);
        $existsGameIds = array_column($internalGames, 'f_external_game_id');

        foreach ($externalProviderGames as $externalGame) {
            $hasExternal = in_array($externalGame['game_code'], $existsGameIds, false);

            if ($hasExternal){
                $this->_update($externalGame, $isTest);
                $countUpdated++;
            }else{
                $this->_insert($externalGame, $isTest);
                $countInserted++;
            }
        }

        return [$countUpdated, $countInserted];
    }

    /**
     * @param array $externalGame
     * @param $isTest
     * @return bool
     * @throws InvalidConfigException
     */
    protected function _update(array $externalGame, $isTest): bool
    {
        $params = $this->_extractGameParams($externalGame);

        /**
         * @var $externalGameCode
         * @var bool $isActive
         * @var string $title
         * @var string $defaultImage
         */
        extract($params, EXTR_OVERWRITE);

        $casinoGame = CasinoGame::findOne(
            [
                'f_provider_code'    => ProviderService::CALETA,
                'f_external_game_id' => $externalGameCode,
                'f_is_test'          => $isTest,
            ]
        );

        if (!$casinoGame) {
            throw new InvalidArgumentException("Update game error: Game $externalGameCode is not found!");
        }

        $casinoGame->f_provider_code     = ProviderService::CALETA;
        $casinoGame->f_external_game_id  = $externalGameCode;
        $casinoGame->f_is_active         = $isActive;
        $casinoGame->f_title             = $title;
        $casinoGame->f_default_title     = $title;
        $casinoGame->f_default_image     = $defaultImage;
        $casinoGame->f_image             = $defaultImage;
        $casinoGame->f_update_date       = date(self::DATE_FORMAT);

        if ($casinoGame->save()){
            return true;
        }

        return false;
    }

    /**
     * @param array $externalGame
     * @param int $isTest
     * @throws InvalidConfigException
     * @throws InvalidArgumentException
     */
    protected function _insert(array $externalGame, int $isTest)
    {
        $order = $this->_getLastOrderNumber(ProviderService::CALETA) + 1;

        $params = $this->_extractGameParams($externalGame);

        /**
         * @var $externalGameCode
         * @var bool $isActive
         * @var string $title
         * @var string $defaultImage
         */
        extract($params, EXTR_OVERWRITE);

        $casinoGame                      = new CasinoGame();
        $casinoGame->f_provider_code     = ProviderService::CALETA;
        $casinoGame->f_external_game_id  = $externalGameCode;
        $casinoGame->f_external_table_id = 0;
        $casinoGame->f_is_active         = $isActive;
        $casinoGame->f_title             = $title;
        $casinoGame->f_default_title     = $title;
        $casinoGame->f_default_image     = $defaultImage;
        $casinoGame->f_image             = '';
        $casinoGame->f_update_date       = date(self::DATE_FORMAT);
        $casinoGame->f_creation_date     = date(self::DATE_FORMAT);
        $casinoGame->f_is_test           = $isTest;
        $casinoGame->f_order             = $order;
        $casinoGame->f_is_new            = 1;

        $gameSuccess = $casinoGame->save();

        if (!$gameSuccess) {
            $this->_logg('Error while adding game ', $externalGameCode);
            throw new InvalidArgumentException('Error while adding game ', $externalGameCode);
        }

        if ($this->_hasDesktopModeInExternalGame($externalGame)){
            $result = $this->_addDeviceToGame($casinoGame->f_id, CasinoGameDevice::TYPE_DESKTOP);
            if (!$result) {
                $this->_logg(
                    'Error while adding devices ' . CasinoGameDevice::TYPE_DESKTOP . ' to game ', $externalGameCode
                );
            }
        }

        if ($this->_hasMobileModeInExternalGame($externalGame)){
            $result = $this->_addDeviceToGame($casinoGame->f_id, CasinoGameDevice::TYPE_MOBILE);
            if (!$result) {
                $this->_logg(
                    'Error while adding devices ' . CasinoGameDevice::TYPE_MOBILE . ' to game ', $externalGameCode
                );
            }
        }

        $category = $this->_mapExternalCategoryToInner($externalGame['category']);
        if ($category){
            $result = $this->_addCategoryToGame($casinoGame->f_id, $category);
            if (!$result) {
                $this->_logg('Error while adding devices to game ', $externalGameCode);
            }
        }else{
            $this->_logg('Unrecognized provider category ' . $externalGame['category'], $externalGameCode);
        }
    }

    /**
     * @param string $externalCategory
     * @return mixed|null
     */
    protected function _mapExternalCategoryToInner(string $externalCategory)
    {
        return self::CATEGORY_MAPPING[$externalCategory] ?? null;
    }

    /**
     * @param array $externalGame
     * @return array
     * @throws InvalidConfigException
     */
    protected function _extractGameParams(array $externalGame): array
    {
        $imageParamName = $this->_getProviderConfigValue(CaletaConstants::CONFIG_FIELD_THUMB_NAME);

        return [
            'externalGameCode' => $externalGame['game_code'],
            'isActive' => (int)$externalGame['enabled'] === 1,
            'title' => $externalGame['name'],
            'defaultImage' => $externalGame[$imageParamName] ?? ''
        ];
    }

    /**
     * @param array $externalGame
     * @return bool
     */
    protected function _hasDesktopModeInExternalGame(array $externalGame): bool
    {
        return
            isset($externalGame['platforms']) && is_array($externalGame['platforms']) &&
            in_array(self::PROVIDER_FIELD_DESKTOP, $externalGame['platforms'], false);
    }

    /**
     * @param array $externalGame
     * @return bool
     */
    protected function _hasMobileModeInExternalGame(array $externalGame): bool
    {
        return
            isset($externalGame['platforms']) && is_array($externalGame['platforms']) &&
            in_array(self::PROVIDER_FIELD_MOBILE, $externalGame['platforms'], false);
    }

    /**
     * @param $isTest
     * @return array
     */
    protected function _getInternalGames($isTest): array
    {
        return CasinoGame::find()
            ->where(['=', 'f_provider_code', ProviderService::CALETA])
            ->andWhere(['=', 'f_is_test', $isTest])
            ->all();
    }

    #region helpers

    /**
     * @param string $provider
     * @return int
     */
    protected function _getLastOrderNumber(string $provider): int
    {
        /** @var ActiveRecord CasinoGame */
        $maxOrderString = CasinoGame::find()
            ->where(['f_provider_code' => ProviderService::CALETA])
            ->orderBy(['f_order' => SORT_DESC])
            ->limit(1)
            ->one();

        if (!$maxOrderString) {
            return -1;
        }

        return $maxOrderString->getAttribute('f_order');
    }

    /**
     * @return array|mixed
     * @throws InvalidConfigException
     * @throws InvalidArgumentException
     */
    protected function _loadGameList()
    {
        $data = [
            'operator_id' => $this->_getOperatorId()
        ];

        $signature = $this->_createSignature($data);
        $url = $this->_getUpdateGameUrl();

        $response = ($this->getHttpClient())->createRequest()
            ->setMethod('POST')
            ->setFormat(Client::FORMAT_JSON)
            ->setHeaders([
                'X-Auth-Signature' => $signature,
                'Content-Type' => 'application/json'
            ])
            ->setUrl($url)
            ->setData($data)
            ->send();

        if (!$this->_validateResponse($response)){
            throw new InvalidArgumentException("Can not get list game from " . ProviderService::CALETA);
        }

        return $response->getData();
    }

    /**
     * @param Response $response
     * @return bool
     */
    protected function _validateResponse(Response $response): bool
    {
        return !(!$response->isOk || !is_array($response->data) || empty($response->data));
    }

    /**
     * @param array $data
     * @return string
     * @throws InvalidConfigException
     */
    protected function _createSignature(array $data): string
    {
        $decodedData = json_encode($data);

        $private_key = $this->_getPrivateKey();
        $signature = "";
        $algo = "SHA256";
        openssl_sign($decodedData, $signature, $private_key, $algo);

        return base64_encode($signature);
    }

    /**
     * @param string $message
     * @param null $id
     * @throws InvalidConfigException
     */
    protected function _logg(string $message, $id = null)
    {
        $logger = $this->getLogger();
        $logger->info($message . ' ' , [$id]);
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    protected function _getOperatorId(): string
    {
        return $this->_getProviderConfigValue(CaletaConstants::CONFIG_FIELD_OPERATOR_ID);
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    protected function _getUpdateGameUrl(): string
    {
        return $this->_getProviderConfigValue(CaletaConstants::CONFIG_FIELD_UPDATE_GAME_URL);
    }

    /**
     * @return resource
     * @throws InvalidConfigException
     */
    protected function _getPrivateKey()
    {
        $fileKey = $this->_getProviderConfigValue(CaletaConstants::CONFIG_FIELD_PRIVATE_KEY);
        $fileKey = preg_replace('@(?<!(GIN|RSA|ATE|KEY|END|LIC))\s@', "\n", $fileKey);

        $key = openssl_pkey_get_private($fileKey);

        if (!$key){
            throw new InvalidConfigException("Key file is wrong");
        }

        return $key;
    }

    #endregion helpers

    #endregion methods
}