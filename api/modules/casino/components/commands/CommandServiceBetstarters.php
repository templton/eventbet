<?php

namespace app\modules\casino\components\commands;

use app\modules\casino\components\ProviderService;
use Enterra\Casino\Common\JsonWebTokenTrait;
use Exception;
use RuntimeException;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Class CommandServiceBetstarters
 *
 * @package app\modules\casino\components\commands
 */
class CommandServiceBetstarters extends CommandServiceAbstract
{
    #region use

    use JsonWebTokenTrait;

    #endregion use

    #region constants

    const CONFIG_URL        = 'url';
    const CONFIG_PARTNER_ID = 'partnerId';

    #endregion constants

    #region properties

    /**
     * @var string
     */
    private static $url = 'games';

    /**
     * @var Client
     */
    private $httpClient;

    #endregion properties

    #region methods

    #region getters

    /**
     * @return Client
     * @throws InvalidConfigException
     */
    public function getHttpClient(): Client
    {
        if (null === $this->httpClient) {
            $providerCfg = $this->getProviderConfig();

            /** @var Client $client */
            $client = Yii::$app->get(Client::class);

            $this->httpClient = clone $client;

            $this->httpClient->baseUrl = trim($providerCfg[self::CONFIG_URL], ' /');
        }

        return $this->httpClient;
    }

    #endregion getters

    #region helpers

    /**
     * @return bool
     * @throws Exception
     */
    public function _updateGames(): bool
    {
        $games = $this->_loadGames();

        if (empty($games)) {
            $this->getLogger()->info('Empty games list received. Exiting.');

            return false;
        }

        $result = true;

        foreach ($games as $game) {
            $result = $result && $this->_updateGame($game);
        }

        return !$result;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function _loadGames(): array
    {
        $response = $this->_sendRequest();

        if ($response->getIsOk()) {
            return $response->getData()['data'] ?? [];
        }

        throw new RuntimeException('Cannot fetch games list');
    }

    /**
     * @return Response
     * @throws InvalidConfigException
     * @throws Exception
     */
    private function _sendRequest(): Response
    {
        $client      = $this->getHttpClient();
        $providerCfg = $this->getProviderConfig();
        $key         = $this->getProviderConfig()[self::CONFIG_PARTNER_ID];
        $data        = [];

        $request = $client->get(
            static::$url,
            $data,
            [
                'Authorization' => $this->generateJWT($key, $data),
                'PartnerToken'  => $providerCfg[self::CONFIG_PARTNER_ID],
                'accept'        => 'application/json',
            ]
        );

        return $request->send();
    }

    /**
     * @param $game
     *
     * @return bool
     */
    private function _updateGame($game): bool
    {
        // @todo Does nothing as games list is empty all the time
        return true;
    }

    #endregion helpers

    #endregion methods
}
