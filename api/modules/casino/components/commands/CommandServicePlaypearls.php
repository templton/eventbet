<?php

namespace app\modules\casino\components\commands;

use app\common\httpClient\HttpClientTrait;
use app\models\casino\CasinoCategory;
use app\models\casino\CasinoGame;
use app\models\casino\CasinoGameCategory;
use app\models\casino\CasinoGameDevice;
use app\modules\casino\components\ProviderService;
use Enterra\Casino\Common\CasinoException;
use RuntimeException;
use yii\db\ActiveRecord;
use yii\base\InvalidConfigException;
use yii\httpclient\Response;

/**
 * Class CommandServicePlaypearls
 *
 * @package app\modules\casino\components\commands
 */
class CommandServicePlaypearls extends CommandServiceAbstract
{
    #region use

    use HttpClientTrait;

    #endregion use

    #region constants

    const REQUEST_AUTHORIZATION = 'Authorization';

    const CONFIG_SITE_ID         = 'siteId';
    const CONFIG_SYSTEM_PASSWORD = 'systemPassword';

    const RESPONSE_ERROR_CODE        = 'errorCode';
    const RESPONSE_ERROR_DESCRIPTION = 'errorDescription';
    const RESPONSE_GAMES             = 'games';


    const ID     = 'id';
    const NAME   = 'name';
    const TYPE   = 'type';
    const STATUS = 'status';


    #endregion constants

    #region properties

    /**
     * @var array
     */
    private $_receivedData = [];

    #endregion properties

    #region methods

    #region helpers

    /**
     * @return bool
     *
     * @throws CasinoException
     * @throws InvalidConfigException
     * @throws \Throwable
     */
    protected function _updateGames(): bool
    {
        $games               = $this->_loadGames();
        $this->_receivedData = $games;

        if (empty($games)) {
            $this->getLogger()->info('Empty games list received. Exiting.');

            return false;
        }

        return $this->_update();
    }

    /**
     * @return array
     * @throws CasinoException
     * @throws InvalidConfigException
     */
    private function _loadGames(): array
    {
        $response = $this->_sendRequest();

        $validateResult = $this->_validateResponse($response);
        if (!$validateResult) {
            throw new RuntimeException('Cannot validate the response');
        }

        return $response->getData()[self::RESPONSE_GAMES];
    }

    /**
     * @return Response
     *
     * @throws InvalidConfigException|CasinoException
     */
    protected function _sendRequest(): Response
    {
        $url = $this->_getGamesUpdatingUrl();

        $siteId         = $this->_getProviderConfigValue(self::CONFIG_SITE_ID);
        $systemPassword = $this->_getProviderConfigValue(self::CONFIG_SYSTEM_PASSWORD);

        $client = $this->getHttpClient();
        $data   = [
        ];

        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($url)
            ->setHeaders([self::REQUEST_AUTHORIZATION => 'Basic ' . base64_encode("$siteId:$systemPassword")])
            ->setData($data)
            ->send();

        return $response;
    }

    /**
     * @return bool
     *
     * @throws InvalidConfigException|\Throwable
     */
    protected function _update(): bool
    {
        $provider = $this->getProviderName();

        $receivedData = $this->_receivedData;

        $currentGames    = $this->_getGames();
        $currentGamesIds = array_column($currentGames, 'providerGameId');

        $receivedGames = [];
        $addedGames    = [];
        $updatedGames  = [];
        $deletedGames  = [];

        foreach ($receivedData as $game) {
            $receivedGames[] = $game;

            $gameId   = $game[self::ID];
            $gameName = $game[self::NAME];
            $gameType = $game[self::TYPE];

            if ($gameName === 'Test Game') {
                continue;
            }

            if (in_array($gameId, $currentGamesIds, false)) {
                $updatedGames[] = $game;

                if (!$this->update(
                    $provider,
                    $gameId,
                    $gameName,
                    1
                )) {
                    continue;
                }

            } else {
                $addedGames[] = $game;

                if (!$this->insert(
                    $provider,
                    $gameId,
                    $gameName,
                    1,
                    $gameType
                )) {
                    continue;
                }
            }
        }

        foreach ($currentGames as $game) {
            if (!in_array($game['providerGameId'], array_column($addedGames, 'id')) &&
                !in_array($game['providerGameId'], array_column($updatedGames, 'id'))
            ) {
                $deletedGames[] = $game;

                if (!$this->update(
                    $this->getProviderName(),
                    $game['providerGameId'],
                    $game['title'],
                    0
                )) {
                    continue;
                }
            }
        }

        $logger = $this->getLogger();

        $logger->info('Update games result:');
        $logger->info('Received: ', array_values(array_column($receivedGames, 'id')));
        $logger->info('Added: ', array_values(array_column($addedGames, 'id')));
        $logger->info('Updated: ', array_values(array_column($updatedGames, 'id')));
        $logger->info('Deleted: ', array_values(array_column($deletedGames, 'providerGameId')));

        return true;
    }

    /**
     * @param Response $response
     *
     * @return array|false
     *
     * @throws InvalidConfigException
     */
    protected function _validateResponse(Response $response): bool
    {
        if (!$response->getIsOk()) {
            $this->_logResponseError('Response error: ', $response);

            return false;
        }

        $data = $response->getData();

        if (empty($data)) {
            $this->_logResponseError('Response data empty: ', $response);

            return false;
        }

        if (!isset($data[self::RESPONSE_GAMES]) || isset($data[self::RESPONSE_ERROR_CODE])) {
            $this->_logResponseError(self::RESPONSE_GAMES . ' not found in response data: ', $response);

            return false;
        }

        return true;
    }

    /**
     * @param string $message
     * @param Response $response
     *
     * @throws InvalidConfigException
     */
    protected function _logResponseError(string $message, Response $response)
    {
        $logger = $this->getLogger();
        $data   = $response->getData();

        $logger->error(
            $message . print_r(['code' => $data[self::RESPONSE_ERROR_CODE], 'message' => $data[self::RESPONSE_ERROR_DESCRIPTION], 'content' => $response->getContent()], true)
        );
    }

    #region configs

    /**
     * @return int|string
     * @throws CasinoException|InvalidConfigException
     */
    protected function getConfigUrl()
    {
        $url = $this->_getProviderConfigValue('url');

        if (substr($url, (strlen($url) - 1), 1) == '/') {
            return $url;
        }

        return $url . '/';
    }

    /**
     * @return string
     *
     * @throws InvalidConfigException|CasinoException
     */
    protected function _getGamesUpdatingUrl(): string
    {
        return $this->getConfigUrl() . 'api/games';
    }

    #endregion configs

    /**
     * @param string $provider
     * @param string $externalGameId
     * @param string $titleGame
     * @param int $isActive
     * @return bool
     */
    private function update(
        string $provider,
        string $externalGameId,
        string $titleGame,
        int $isActive
    ): bool
    {
        /** @var ActiveRecord $casinoGames */
        $casinoGames = CasinoGame::find()
            ->where(['f_provider_code' => $provider, 'f_external_game_id' => $externalGameId])
            ->all();

        if (!$casinoGames) {
            return false;
        }
        foreach ($casinoGames as $casinoGame) {
            $casinoGame->f_provider_code    = $provider;
            $casinoGame->f_external_game_id = $externalGameId;
            $casinoGame->f_is_active        = $isActive;
            $casinoGame->f_title            = $titleGame;
            $casinoGame->f_default_title    = $titleGame;
            $casinoGame->f_update_date      = date('Y-m-d H:i:s');

            if (!$casinoGame->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $provider
     * @param string $externalGameId
     * @return bool
     * @throws \Throwable
     */
    private function delete(string $provider, string $externalGameId): bool
    {
        /** @var ActiveRecord $casinoGames */
        $casinoGames = CasinoGame::find()
            ->where(['f_provider_code' => $provider, 'f_external_game_id' => $externalGameId])
            ->all();

        if (!$casinoGames) {
            return false;
        }

        foreach ($casinoGames as $casinoGame) {
            if (!$casinoGame->delete()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $provider
     * @param string $externalGameId
     * @param string $titleGame
     * @param int $isActive
     * @param string $category
     * @return bool
     * @throws InvalidConfigException|\Throwable
     */
    private function insert(
        string $provider,
        string $externalGameId,
        string $titleGame,
        int $isActive,
        string $category
    ): bool
    {
        /** @var ActiveRecord $casinoGames */
        $casinoGames = CasinoGame::find()
            ->where(['f_provider_code' => $provider, 'f_external_game_id' => $externalGameId])
            ->all();

        if($casinoGames) {
            $this->delete($provider, $externalGameId);
        }

        $maxOrder = $this->_getLastOrderNumber($provider);

        $result = $this->_insert(
            $provider,
            $externalGameId,
            $titleGame,
            $isActive,
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
            0,
            $maxOrder + 1,
            1,
            $category
        );

        if (!$result) {
            return false;
        }

        $result = $this->_insert(
            $provider,
            $externalGameId,
            $titleGame,
            $isActive,
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
            1,
            $maxOrder + 2,
            1,
            $category
        );

        if (!$result) {
            return false;
        }

        return $result;
    }

    /**
     * @param string $provider
     * @param string $externalGameId
     * @param string $titleGame
     * @param int $isActive
     * @param string $createDate
     * @param string $updateDate
     * @param int $mode
     * @param int $order
     * @param int $isNew
     * @param string $category
     * @return bool
     * @throws InvalidConfigException
     */
    private function _insert(
        string $provider,
        string $externalGameId,
        string $titleGame,
        int $isActive,
        string $createDate,
        string $updateDate,
        int $mode,
        int $order,
        int $isNew,
        string $category
    ): bool
    {
        $logger    = $this->getLogger();
        $imageName = $this->_getGameImageName($provider, $titleGame);

        /** @var ActiveRecord CasinoGame */
        $casinoGame                      = new CasinoGame();
        $casinoGame->f_provider_code     = $provider;
        $casinoGame->f_external_game_id  = $externalGameId;
        $casinoGame->f_external_table_id = 0;
        $casinoGame->f_is_active         = $isActive;
        $casinoGame->f_title             = $titleGame;
        $casinoGame->f_default_title     = $titleGame;
        $casinoGame->f_default_image     = $imageName;
        $casinoGame->f_image             = $imageName;
        $casinoGame->f_update_date       = $createDate;
        $casinoGame->f_creation_date     = $updateDate;
        $casinoGame->f_is_test           = $mode;
        $casinoGame->f_order             = $order;
        $casinoGame->f_is_new            = $isNew;

        $gameSuccess = $casinoGame->save();
        if (!$gameSuccess) {
            $logger->info('Error while adding game ', $externalGameId);
        }

        $result = $this->_addDeviceToGame($casinoGame->f_id, 'desktop');
        if (!$result) {
            $logger->info('Error while adding devices to game ', $externalGameId);
        }

        $result = $this->_addDeviceToGame($casinoGame->f_id, 'mobile');
        if (!$result) {
            $logger->info('Error while adding devices to game ', $externalGameId);
        }

        $result = $this->_addCategoryToGame($casinoGame->f_id, $category);
        if (!$result) {
            $logger->info('Error while adding devices to game ', $externalGameId);
        }

        return $gameSuccess;
    }

    /**
     * @param string $provider
     * @param string $gameName
     * @return string
     */
    protected function _getGameImageName(string $provider, string $gameName): string
    {
        $gameName = str_replace(' ', '', $gameName); // Replaces all spaces with hyphens.
        $gameName = preg_replace('/[^A-Za-z0-9\-]/', '', $gameName); // Removes special chars.
        return '/' . $provider . '/' . strtolower($gameName) . '.png';
    }

    #endregion helpers

    #endregion methods
}