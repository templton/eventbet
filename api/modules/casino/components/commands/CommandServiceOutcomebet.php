<?php


namespace app\modules\casino\components\commands;

use app\models\casino\CasinoGame;
use app\models\casino\CasinoGameDevice;
use app\modules\casino\components\helpers\ServiceHelper;
use Enterra\Casino\ApiSdk\CasinoSdkApiOutcomebet;
use Enterra\Casino\Constants\Outcomebet\Common as Constants;
use Enterra\OutcomebetClient\Client;
use Exception;
use InvalidArgumentException;
use UnexpectedValueException;
use yii\base\InvalidConfigException;

/**
 * Class CommandServiceOutcomebet
 *
 * @package app\modules\casino\components\commands
 */
class CommandServiceOutcomebet extends CommandServiceAbstract
{
    #region constants

    const PROVIDERS_MAP           = [
        Constants::PROVIDER_GREENTUBE => Constants::PROVIDER_NOVOMATIC,
        Constants::PROVIDER_GAMINATOR => Constants::PROVIDER_NOVOMATIC,
    ];

    #endregion

    #region properties

    /**
     * @var Client
     */
    private $_client;

    #endregion

    #region methods

    /**
     * @throws Exception
     */
    protected function _updateGames(): bool
    {
        try {
            $this->_update($this->_getExternalGames());
        } catch (Exception $e) {
            $this->getLogger()->error($e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @throws Exception
     */
    private function _getClient(): Client
    {
        if (!$this->_client) {
            /** @var CasinoSdkApiOutcomebet $sdk */
            $sdk = ServiceHelper::getApiSdk($this->getProviderName());

            $this->_client = $sdk->getClient();
        }

        return $this->_client;
    }

    /**
     * @param array $receivedData
     *
     * @throws Exception
     */
    private function _update(array $receivedData) //NOSONAR
    {
        if (!array_key_exists('Games', $receivedData)) {
            throw new UnexpectedValueException('Received data is not contained games!');
        }

        $externalGames          = $this->_formatReceivedData($receivedData);
        $statisticInsertedCount = 0;
        $statisticInsertFailed  = [];
        $statisticUpdatedCount  = 0;
        $statisticUpdateFailed  = [];
        $statisticMarkDeleted   = [];
        foreach ($externalGames as $provider => $games) {
            if ($this->_formatToSubProviderName($provider) !== $this->getProviderName()) {
                continue;
            }

            $internalGames    = $this->_getInternalGamesByProvider($provider);
            $externalGamesIds = array_column($games, 'Id');
            $internalGamesIds = array_column($internalGames, 'f_external_game_id');

            foreach ($games as $externalGame) {
                /**
                 * @var string $Id
                 * @var string $Name
                 * @var string $Type
                 * @var string $Format
                 */
                extract($externalGame, EXTR_OVERWRITE);

                if ($Format !== 'html') {
                    continue;
                }

                $hasInternal = in_array($externalGame['Id'], $internalGamesIds, true);
                try {
                    if (!$hasInternal) {
                        $this->_insertGame($Id, $provider, $Name, $Type, 1) &&
                        $this->_insertGame($Id, $provider, $Name, $Type, 0);
                        $statisticInsertedCount++;
                    } else {
                        $this->_updateGame($Id, $provider, 1, 0, $Name) &&
                        $this->_updateGame($Id, $provider, 0, 0, $Name);
                        $statisticUpdatedCount++;
                    }
                } catch (Exception $e) {
                    if ($hasInternal) {
                        $statisticUpdateFailed[] = $externalGame;
                    } else {
                        $statisticInsertFailed[] = $externalGame;
                    }
                    $this->getLogger()->error($e->getMessage());
                }
            }

            // Mark to deleted
            foreach (array_diff($internalGamesIds, $externalGamesIds) as $externalId) {
                $this->_updateGame($externalId, $provider, 1, 1) &&
                $this->_updateGame($externalId, $provider, 0, 1);
                $statisticMarkDeleted[] = $externalId;
            }
        }

        $this->getLogger()->info('Update games result:');
        $this->getLogger()->info('Added: ', [$statisticInsertedCount]);
        $this->getLogger()->info('Updated: ', [$statisticUpdatedCount]);
        $this->getLogger()->info('Deleted: ', $statisticMarkDeleted);
        $this->getLogger()->info('Insert failed: ', $statisticInsertFailed);
        $this->getLogger()->info('Update failed: ', $statisticUpdateFailed);
    }

    /**
     * @param string $externalId
     * @param string $provider
     * @param string $title
     * @param string $type
     * @param int    $isTest
     *
     * @return bool
     * @throws InvalidConfigException
     */
    private function _insertGame(string $externalId, string $provider, string $title, string $type, int $isTest): bool
    {
        $image    = $this->_getGameImageName($provider, $externalId);
        $provider = $this->_formatToSubProviderName($provider);
        $order    = $this->_getLastOrderNumber($provider) + 1;

        $casinoGame                      = new CasinoGame();
        $casinoGame->f_provider_code     = $provider;
        $casinoGame->f_external_game_id  = $externalId;
        $casinoGame->f_external_table_id = 0;
        $casinoGame->f_is_active         = false;
        $casinoGame->f_title             = $title;
        $casinoGame->f_default_title     = $title;
        $casinoGame->f_default_image     = $image;
        $casinoGame->f_image             = $image;
        $casinoGame->f_update_date       = date('Y-m-d H:i:s');
        $casinoGame->f_creation_date     = date('Y-m-d H:i:s');
        $casinoGame->f_is_test           = $isTest;
        $casinoGame->f_order             = $order;
        $casinoGame->f_is_new            = 1;

        $gameSuccess = $casinoGame->save();

        if (!$gameSuccess) {
            $this->getLogger()->info('Error while adding game ' . $externalId);

            return false;
        }

        $result = $this->_addDeviceToGame($casinoGame->f_id, CasinoGameDevice::TYPE_DESKTOP);
        if (!$result) {
            $this->getLogger()->info('Error while adding devices to game ' . $externalId);
        }

        $result = $this->_addDeviceToGame($casinoGame->f_id, CasinoGameDevice::TYPE_MOBILE);
        if (!$result) {
            $this->getLogger()->info('Error while adding devices to game ' . $externalId);
        }

        $result = $this->_addCategoryToGame($casinoGame->f_id, $type);
        if (!$result) {
            $this->getLogger()->info('Error while adding devices to game ' . $externalId);
        }

        return $gameSuccess;
    }

    /**
     * @param string $externalId
     * @param string $provider
     * @param int    $isTest
     * @param int    $isDeleted
     * @param string $title
     *
     * @return bool
     * @throws Exception
     */
    private function _updateGame(
        string $externalId,
        string $provider,
        int $isTest,
        int $isDeleted,
        string $title = ''
    ): bool {
        $provider = $this->_formatToSubProviderName($provider);
        $game     = CasinoGame::findOne(
            [
                'f_provider_code'    => $provider,
                'f_external_game_id' => $externalId,
                'f_is_test'          => $isTest,
            ]
        );

        if (!$game) {
            throw new InvalidArgumentException("Update game error: Game '{$externalId}' is not found!");
        }

        $title && $game->f_title = $title;
        $game->f_is_deleted = $isDeleted;

        return $game->save();
    }

    /**
     * @param string $subProvider
     * @param string $gameId
     *
     * @return string
     */
    private function _getGameImageName(string $subProvider, string $gameId): string
    {
        return '/' . $this->_providerName . '/' . $subProvider . '/' . strtolower($gameId) . '.jpg';
    }

    /**
     * @param string $provider
     *
     * @return false|mixed
     * @throws InvalidConfigException
     */
    private function _getInternalGamesByProvider(string $provider)
    {
        $runtimeCache = ServiceHelper::getArrayCacheService();
        $key          = md5(__CLASS__ . $this->_formatToSubProviderName($provider));
        if (!$runtimeCache->get($key)) {
            $runtimeCache->set(
                $key,
                CasinoGame::find()
                    ->where(['=', 'f_provider_code', $this->_formatToSubProviderName($provider)])
                    ->andWhere(['=', 'f_is_test', 0])
                    ->all()
            );
        }

        return $runtimeCache->get($key);
    }

    /**
     * @return array
     * @throws InvalidConfigException
     * @throws Exception
     */
    private function _getExternalGames(): array
    {
        $runtimeCache = ServiceHelper::getArrayCacheService();
        $key          = md5(__CLASS__ . '_outcomebet_external_games');
        if (!$runtimeCache->get($key)) {
            $runtimeCache->set(
                $key,
                $this->_getClient()->listGames()
            );
        }

        return $runtimeCache->get($key) ?? [];
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    private function _formatReceivedData(array $data): array
    {
        return array_reduce(
            $data['Games'],
            static function ($acc, $cur) {
                $provider = $cur['SectionId'];

                if (!array_key_exists($provider, $acc)) {
                    $acc[$provider] = [$cur];
                } else {
                    $acc[$provider][] = $cur;
                }

                return $acc;
            },
            []
        );
    }

    /**
     * @param string $subProvider
     *
     * @return string
     */
    private function _formatToSubProviderName(string $subProvider): string
    {
        $subProvider .= '_' . Constants::PROVIDER_NAME;
        if (array_key_exists($subProvider, self::PROVIDERS_MAP)) {
            $subProvider = self::PROVIDERS_MAP[$subProvider];
        }

        return $subProvider;
    }

    #endregion

}