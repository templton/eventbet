<?php

namespace app\modules\casino\components\commands;

use app\modules\casino\components\adapters\ZendLoggerAdapter;
use app\modules\casino\components\callbacks\CallbackServiceEzugi;
use app\modules\casino\components\helpers\CacheHelper;
use app\modules\casino\components\ProviderService;
use Devristo\Phpws\Client\WebSocket;
use Devristo\Phpws\Exceptions\WebSocketInvalidUrlScheme;
use Devristo\Phpws\Messaging\MessageInterface;
use Enterra\Casino\Repository\{Table\CasinoTableManagerInterface,
    Table\CasinoTableManagerYii,
    Table\CasinoTableModel,
    TableLimit\CasinoTableLimitManagerInterface,
    TableLimit\CasinoTableLimitManagerYii,
    TableLimit\CasinoTableLimitModel};
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory;
use React\EventLoop\Timer\TimerInterface;
use RuntimeException;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Class CommandServiceEzugi
 *
 * @package app\modules\casino\components\commands
 */
class CommandServiceEzugi extends CommandServiceAbstract
{
    #region constants

    const CONFIG_TABLES_UPDATING_URL = 'tablesUpdatingUrl';
    const CONFIG_OPERATOR_ID         = 'operatorId';

    const FIELD_TABLE_ID = 'f_external_table_id';

    #endregion constants

    #region properties

    /** @var array */
    protected $receivedTables = [];
    /** @var array */
    protected $updatedTables = [];
    /** @var array */
    protected $removedTables = [];
    /** @var array */
    protected $addedTables = [];
    /** @var array */
    protected $notAddedTables = [];

    /** @var array */
    protected $receivedLimits = [];
    /** @var array */
    protected $updatedLimits = [];
    /** @var array */
    protected $removedLimits = [];
    /** @var array */
    protected $addedLimits = [];
    /** @var array */
    protected $notAddedLimits = [];

    protected $currentTablesList;

    #endregion properties

    #region methods

    #region helpers

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    protected function _updateGames(): bool
    {
        return $this->_loadTablesInfo();
    }

    #region configs

    /**
     * @return string
     * @throws InvalidConfigException
     */
    protected function getAddress(): string
    {
        $config = $this->getProviderConfig();
        if (!isset($config[self::CONFIG_TABLES_UPDATING_URL])) {
            throw new RuntimeException(
                self::CONFIG_TABLES_UPDATING_URL . '  not found in provider config: providerName = ' .
                $this->getProviderName()
            );
        }

        return $config[self::CONFIG_TABLES_UPDATING_URL];
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    protected function getOperatorId(): string
    {
        $config = $this->getProviderConfig();
        if (!isset($config[self::CONFIG_OPERATOR_ID])) {
            throw new RuntimeException(
                self::CONFIG_OPERATOR_ID . '  not found in provider config: providerName = ' .
                $this->getProviderName()
            );
        }

        return $config['operatorId'];
    }

    /**
     * @return string
     */
    protected function getNumberOfTimerTick(): string
    {
        return 2;
    }

    /**
     * @return string
     */
    protected function getTokenPath(): string
    {
        return CallbackServiceEzugi::DEFAULT_TOKEN_PATH;
    }

    #endregion configs

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    protected function _loadTablesInfo(): bool
    {
        try {
            if (!ProviderService::isActive($this->getProviderName())) {
                return true;
            }

            $logger = $this->getLogger();

            $this->receivedTablesAndLimits();

            if (count($this->receivedTables) === 0
                && count($this->receivedLimits) === 0
            ) {
                $logger->info('Error getting information about tables');
            } else {
                $this->updateTables();
                $this->updateLimits();
                $this->log();

                CacheHelper::deleteIfExist(CacheHelper::CASINO_GAME_LIST);
            }

            return true;
        } catch (\Exception $e) {
            $this->getLogger()->error($e);

            return false;
        }
    }

    /**
     * @return CasinoTableModel[]
     *
     * @throws InvalidConfigException
     */
    protected function getTables(): array
    {
        return $this->_getTables();
    }

    /**
     * @return CasinoTableLimitModel[]
     *
     * @throws InvalidConfigException
     */
    protected function getLimits(): array
    {
        return $this->_getLimits();
    }

    /**
     * @return array
     *
     * @throws InvalidConfigException
     */
    protected function getGames(): array
    {
        return $this->_getGames();
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    protected function getCurrencies(): array
    {
        $currencyInfo = $this->getCurrencyInfo();

        $currencies = [];
        foreach ($currencyInfo->getCurrencies(true) as $currency) {
            if ($currency[$currencyInfo::FIELD_USE_FOR_CASINO]) {
                $currencies[] = $currency[$currencyInfo::FIELD_ISO_CODE];
            }
        }

        if (count($currencies) <= 0) {
            $this->getLogger()->info('No active currencies used in the casino');
        }

        return $currencies;
    }

    /**
     * @param string $tableId
     * @param string $providerGameId
     * @param array $condition
     *
     * @return mixed|null
     *
     * @throws InvalidConfigException
     */
    protected function getGame(string $tableId, string $providerGameId, array $condition = null)
    {
        return $this->_getGame($tableId, $providerGameId, $condition);
    }

    /**
     * @return string
     */
    protected function getToken(): string
    {
        $token = file_get_contents($this->getTokenPath());

        // Remove EOL symbol from token and return
        return trim(preg_replace('/\s\s+/', ' ', $token));
    }

    /**
     * @throws InvalidConfigException
     */
    protected function updateLimits()
    {
        /** @var CasinoTableLimitManagerInterface $limitManager */
        $limitManager = Yii::$app->get(CasinoTableLimitManagerYii::class);
        $casinoTable  = [];
        /**
         * @var string           $providerTableId
         * @var CasinoTableModel $table
         */
        foreach ($this->addedTables as $providerTableId => $table) {
            $casinoTable[$table->getExternalTableId()] = $table;
        }
        /**
         * @var string           $providerTableId
         * @var CasinoTableModel $table
         */
        foreach ($this->updatedTables as $providerTableId => $table) {
            $casinoTable[$table->getExternalTableId()] = $table;
        }

        $limits = $this->getLimits();

        foreach ($this->receivedLimits as $providerLimitId => $providerLimit) {
            if (!array_key_exists(
                $providerLimit['TableId'],
                $this->notAddedTables
            )
            ) {
                $table = $casinoTable[$providerLimit['TableId']];

                if (!array_key_exists($providerLimitId, $limits)) {
                    $result = $limitManager->insert(
                        $table->getId(),
                        $providerLimitId,
                        $providerLimit['Currency'],
                        $providerLimit['Min_Bet'] ?? 0,
                        $providerLimit['Max_Bet'] ?? 0,
                        $providerLimit['MinSideBet'] ?? 0,
                        $providerLimit['MaxSideBet'] ?? 0,
                        $providerLimit['MinBetBehind'] ?? 0,
                        $providerLimit['MaxBetBehind'] ?? 0
                    );

                    if (false !== $result) {
                        $this->addedLimits[$result->getExternalLimitId()] = $result;
                    } else {
                        $this->notAddedLimits[$providerLimitId] = $providerLimit;
                    }
                } else {
                    $result = $limitManager->update(
                        $table->getId(),
                        $providerLimitId,
                        $providerLimit['Currency'],
                        $providerLimit['Min_Bet'] ?? 0,
                        $providerLimit['Max_Bet'] ?? 0,
                        $providerLimit['MinSideBet'] ?? 0,
                        $providerLimit['MaxSideBet'] ?? 0,
                        $providerLimit['MinBetBehind'] ?? 0,
                        $providerLimit['MaxBetBehind'] ?? 0
                    );

                    if (false !== $result) {
                        $this->updatedLimits[$result->getExternalLimitId()] = $result;
                    }
                }
            } else {
                $this->notAddedLimits[$providerLimitId] = $providerLimit;
            }
        }

        /** @var CasinoTableLimitModel $limit */
        foreach ($limits as $providerLimitId => $limit) {
            if (!(array_key_exists($providerLimitId, $this->addedLimits) ||
                array_key_exists($providerLimitId, $this->updatedLimits))
            ) {
                $this->removedLimits[$limit->getExternalLimitId()] = $limit;

                $limitManager->delete(
                    $limit->getCasinoTableId(),
                    $limit->getExternalLimitId()
                );
            }
        }
    }

    /**
     * @throws InvalidConfigException
     */
    protected function updateTables()
    {
        /** @var CasinoTableManagerInterface $tableManager */
        $tableManager = Yii::$app->get(CasinoTableManagerYii::class);

        $doNotDeleteTables = [];
        $tables            = $this->getTables();
        foreach ($this->receivedTables as $providerTableId => $providerTable) {
            $condition      = ['>', self::FIELD_TABLE_ID, 0];
            $gameInfo       = $this->getGame($providerTableId, $providerTable['GameType'], $condition);
            $takenSeats     = 0;
            $availableSeats = 0;
            if (isset($providerTable['AvailableSeats'])) {
                if (is_array($providerTable['AvailableSeats'])) {
                    foreach ($providerTable['AvailableSeats'] as $seats) {
                        if ($seats['SeatId'] != 'd') {
                            if ($seats['Taken']) {
                                $takenSeats++;
                            }
                            $availableSeats++;
                        }
                    }
                }
            }
            if ($gameInfo) {
                if (array_key_exists($providerTableId, $tables)) {
                    $result                                             = $tableManager->update(
                        $gameInfo['provider'],
                        $providerTableId,
                        $gameInfo['providerGameId'],
                        $gameInfo['id'],
                        (int)$providerTable['IsActive'],
                        $providerTable['DealerId'],
                        $providerTable['DealerName'],
                        str_replace(' ', '%20', $providerTable['PictureLink']),
                        $availableSeats,
                        $takenSeats
                    );
                    $this->updatedTables[$result->getExternalTableId()] = $result;
                    $doNotDeleteTables[$result->getExternalTableId()]   = $result;
                } else {
                    $result                                           = $tableManager->insert(
                        $gameInfo['provider'],
                        $providerTableId,
                        $gameInfo['providerGameId'],
                        $gameInfo['id'],
                        (int)$providerTable['IsActive'],
                        $providerTable['DealerId'],
                        $providerTable['DealerName'],
                        str_replace(' ', '%20', $providerTable['PictureLink']),
                        $availableSeats,
                        $takenSeats
                    );
                    $this->addedTables[$result->getExternalTableId()] = $result;
                    $doNotDeleteTables[$result->getExternalTableId()] = $result;
                }
            } else {
                $this->notAddedTables[$providerTableId] = $providerTable;
            }
        }
        /**
         * @var string           $externalTableId
         * @var CasinoTableModel $table
         */
        foreach ($tables as $externalTableId => $table) {
            if (!array_key_exists($externalTableId, $doNotDeleteTables)) {
                $this->removedTables[$externalTableId] = $table;
                $tableManager->delete($table->getProvider(), $table->getExternalTableId());
            }
        }
    }

    /**
     * @throws InvalidConfigException
     * @throws WebSocketInvalidUrlScheme
     */
    protected function receivedTablesAndLimits()
    {
        $address    = $this->getAddress();
        $operatorId = $this->getOperatorId();
        $logger     = $this->getLogger();

        $allTablesInfo = [];
        foreach ($this->getCurrencies() as $currency) {
            $tablesInfo = $this->getTablesInfo($address, $currency, $operatorId, $logger);
            foreach ($tablesInfo as $tableInfo) {
                $tableInfo['Currency'] = $currency;
                $allTablesInfo[]       = $tableInfo;
            }
        }
        $this->loadReceivedTablesAndLimits($allTablesInfo);
    }

    /**
     * @param $allTablesInfo
     *
     * @throws InvalidConfigException
     */
    protected function loadReceivedTablesAndLimits($allTablesInfo)
    {
        $tables = [];
        $limits = [];
        $logger = $this->getLogger();

        foreach ($allTablesInfo as $tableInfo) {
            $tableId    = $tableInfo['TableId'];
            $limitsInfo = $tableInfo['LimitsList'];

            foreach ($limitsInfo as $key => $limitInfo) {
                if (empty($limitInfo['LimitId'])) {
                    $logger->error(
                        'Data corrupted for table #' . $tableInfo['TableId']
                    );
                    continue 2;
                }

                $logger->info('Updating table #' . $tableInfo['TableId']);

                $limitId = $limitInfo['LimitId'];

                if (!array_key_exists($limitId, $limits)) {
                    $limitInfo['TableId']  = $tableId;
                    $limitInfo['Currency'] = $tableInfo['Currency'];
                    $limits[$limitId]      = $limitInfo;
                }
            }

            if (!array_key_exists($tableId, $tables)) {
                unset($tableInfo['LimitsList']);
                $tables[$tableId] = $tableInfo;
            }
        }

        $this->receivedTables = $tables;
        $this->receivedLimits = $limits;
    }

    /**
     * @param string          $address
     * @param string          $currency
     * @param string          $operatorId
     * @param LoggerInterface $logger
     *
     * @return array
     * @throws InvalidConfigException
     * @throws WebSocketInvalidUrlScheme
     */
    protected function getTablesInfo(
        string $address,
        string $currency,
        string $operatorId,
        LoggerInterface $logger
    ): array {
        $this->currentTablesList = [];

        $zendLogger = new ZendLoggerAdapter($logger);

        $logger = $this->getLogger();
        $loop   = Factory::create();
        $client = new WebSocket(
            $address, $loop, $zendLogger
        );

        $logger->info('Currency: ' . $currency);

        $client->on("request", function ($headers) use ($logger, $address) {
            $logger->info('Connect to ' . $address);
        });

        $client->on("handshake", function () use ($logger, $client, $operatorId, $currency) {

        });

        $client->on("message", function ($message) use ($client, $logger, $loop) {
            /** @var MessageInterface $message */
            $msg = json_decode($message->getData(), true);

            if ($msg['MessageType'] == 'ActiveTablesList') {
                $this->currentTablesList = $msg['TablesList'];
                $client->close();
            }
        });

        $client->on("connect", function () use ($loop, $logger, $client, $operatorId, $currency) {
            // Unlink token file
            $tokenPath = $this->getTokenPath();
            if (file_exists($tokenPath)) {
                unlink($tokenPath);
            }

            $client->send(
                '{"MessageType":"InitializeSession","OperatorID":' .
                $operatorId . ',"vipLevel":0,"SessionCurrency":"' . $currency . '"}');

            $that       = $this;
            $tokenTimer = $loop->addPeriodicTimer(1,
                function ($tokenTimer) use ($that, $loop, $client, $operatorId, $currency) {
                    if (file_exists($this->getTokenPath())) {
                        $token = $that->getToken();

                        $message = '{"MessageType":"AuthenticateSession","OperatorID":' .
                            $operatorId . ',"vipLevel":0,"SessionCurrency":"' . $currency . '","Token": "' . $token .
                            '"}';
                        $client->send($message);

                        $loop->cancelTimer($tokenTimer);
                    } else {
                        /** @var TimerInterface $tokenTimer */
                        $timerTick = $tokenTimer->getData();
                        // Number of tries is exceeded
                        if ($timerTick > $this->getNumberOfTimerTick()) {
                            $loop->cancelTimer($tokenTimer);
                            $client->close();

                            $this->getLogger()->info('Number of tries to get token is exceeded. Timer tick: ' .
                                $timerTick);
                        }

                        $tokenTimer->setData($tokenTimer->getData() + 1);
                    }
                });
            // Set initial tick
            $tokenTimer->setData(1);
        });


        $client->open();
        $loop->run();

        return $this->currentTablesList;
    }

    /**
     * @throws InvalidConfigException
     */
    protected function log()
    {
        $logger = $this->getLogger();

        $this->logCount('receivedTables', 'Received tables: ', $logger);
        $this->logCount('addedTables', 'Added tables: ', $logger);
        $this->logCount('updatedTables', 'Updated tables: ', $logger);
        $this->logCount('removedTables', 'Removed tables: ', $logger);
        $this->logCount('notAddedTables', 'Not added tables: ', $logger);
        $this->logCount('receivedLimits', 'Received limits: ', $logger);
        $this->logCount('addedLimits', 'Added limits: ', $logger);
        $this->logCount('updatedLimits', 'Updated limits: ', $logger);
        $this->logCount('removedLimits', 'Removed limits: ', $logger);
        $this->logCount('notAddedLimits', 'Not added limits: ', $logger);
    }

    /**
     * @param string          $name
     * @param string          $message
     * @param LoggerInterface $logger
     */
    protected function logCount(string $name, string $message, LoggerInterface $logger)
    {
        $count = count($this->{$name});
        $logger->info($message . $count);
        if ($count) {
            $str = '';
            foreach ($this->{$name} as $id => $value) {
                $str = $str . $id . ' ';
            }
            $logger->info($str);
        }
    }

    #endregion helpers

    #endregion methods
}
