<?php

namespace app\modules\casino\components\commands;

use app\common\httpClient\HttpClientTrait;
use app\modules\casino\components\ProviderService;
use BadMethodCallException;
use Enterra\Casino\Repository\Table\CasinoTableManagerInterface;
use Enterra\Casino\Repository\Table\CasinoTableManagerYii;
use Enterra\Casino\Repository\Table\CasinoTableModel;
use Enterra\Casino\Repository\TableLimit\CasinoTableLimitManagerInterface;
use Enterra\Casino\Repository\TableLimit\CasinoTableLimitManagerYii;
use Enterra\Casino\Repository\TableLimit\CasinoTableLimitModel;
use Yii as Yii;
use yii\base\InvalidConfigException;
use yii\httpclient\Exception;
use yii\httpclient\Response;

/**
 * Class CommandServiceVivo
 *
 * @package app\modules\casino\components\commands
 */
class CommandServiceVivo extends CommandServiceAbstract
{
    #region use

    use HttpClientTrait;

    #endregion use

    #region constants

    const CONFIG_OPERATOR_ID        = 'operatorId';
    const CONFIG_GAMES_UPDATING_URL = 'gamesUpdatingUrl';

    const GAMES_NAME = [
        'baccarat',
        'roulette',
        'blackjack',
        'poker',
    ];

    const GAME_TABLES_ID = [
        '1'     => ['1', '43', '233', '245', '244', '183', '231', '232', '26', '229', '36', '13', '230', '177', '167', '266'],
        '5'     => ['182', '168'],
        '2'     => ['3', '28', '27', '180', '181', '154', '155', '156', '157', '158', '44', '45', '46', '47', '239',
                    '240', '241', '242', '243', '14', '17', '21', '160', '161', '162', '163', '272', '273'],
        '4'     => ['16', '18', '212', '222'],
        '202'   => ['256'],
        'lobby' => ['lobby', 'gmt-lobby'],
    ];

    const REQUEST_GAME_NAME       = 'GameName';
    const REQUEST_OPERATOR_ID     = 'OperatorId';
    const REQUEST_PLAYER_CURRENCY = 'PlayerCurrency';

    const RESPONSE_GAME_DATA   = 'gameData';
    const RESPONSE_TABLES      = 'tables';
    const RESPONSE_TABLE_ID    = 'tableId';
    const RESPONSE_LIMITS      = 'limits';
    const RESPONSE_LIMIT_ID    = 'limitid';
    const RESPONSE_OPEN        = 'open';
    const RESPONSE_DEALER_NAME = 'dealerName';
    const RESPONSE_LIMIT_MIN   = 'limitMin';
    const RESPONSE_LIMIT_MAX   = 'limitMax';

    const ID     = 'id';
    const TABLE  = 'table';
    const LIMITS = 'limits';

    const FIELD_TABLE_ID = 'f_external_table_id';

    #endregion constants

    #region properties

    /**
     * @var array
     */
    private $_receivedData = [];

    #endregion properties

    #region methods

    #region helpers

    /**
     * @return bool
     *
     * @throws InvalidConfigException
     *
     * @throws Exception
     */
    protected function _updateGames(): bool
    {
        $currencyRepo = $this->getCurrencyInfo();

        $result = true;

        $currencies = $currencyRepo->getCurrencies(true);
        foreach ($currencies as $currency) {
            if ($currency[$currencyRepo::FIELD_USE_FOR_CASINO]) {
                $currencyIso = $currency[$currencyRepo::FIELD_ISO_CODE];
                if (!$this->_requestGamesByCurrency($currencyIso)) {
                    $result = false;
                }
            }
        }

        if (!$this->_update()) {
            $result = false;
        }

        return $result;
    }

    /**
     * @param string $currencyIso
     *
     * @return bool
     *
     * @throws InvalidConfigException
     * @throws Exception
     */
    protected function _requestGamesByCurrency(string $currencyIso): bool
    {
        $result = true;

        $logger = $this->getLogger();

        $gamesName = $this->_getGamesName();
        foreach ($gamesName as $gameName) {
            $logger->info("Request games: currency = $currencyIso, gameName = $gameName");

            $response = $this->_sendRequest($currencyIso, $gameName);

            $validateResult = $this->_validateResponse($response);
            if (!$validateResult) {
                $result = false;

                continue;
            }

            $data     = $response->getData();
            $gameData = $data[self::RESPONSE_GAME_DATA];
            $tables   = $gameData[self::RESPONSE_TABLES];
            /** @var array $table */
            foreach ($tables as $table) {
                $providerTableId = $table[self::RESPONSE_TABLE_ID];
                if (isset($table[self::RESPONSE_LIMITS])) {
                    $limits = $table[self::RESPONSE_LIMITS];
                    unset($table[self::RESPONSE_LIMITS]);
                } else {
                    $limits = [];
                }

                if (!isset($this->_receivedData[$providerTableId][self::TABLE])) {
                    $this->_receivedData[$providerTableId][self::TABLE] = $table;
                }

                if (!empty($limits)) {
                    $this->_receivedData[$providerTableId][self::LIMITS][$currencyIso] = $limits;
                }
            }
        }

        return $result;
    }

    /**
     * @return bool
     *
     * @throws InvalidConfigException
     */
    protected function _update(): bool
    {
        /** @var CasinoTableManagerInterface $tableRepository */
        $tableRepository = Yii::$app->get(CasinoTableManagerYii::class);
        /** @var CasinoTableLimitManagerInterface $limitRepository */
        $limitRepository = Yii::$app->get(CasinoTableLimitManagerYii::class);

        $provider = $this->getProviderName();

        $receivedData  = $this->_receivedData;
        $currentTables = $this->_getTables();
        $currentLimits = $this->_getLimits();

        $receivedTables   = [];
        $addedTables      = [];
        $updatedTables    = [];
        $deletedTables    = [];
        $unaffectedTables = [];

        $receivedLimits   = [];
        $addedLimits      = [];
        $updatedLimits    = [];
        $deletedLimits    = [];
        $unaffectedLimits = [];

        foreach ($receivedData as $providerTableId => $data) {
            $table = $data[self::TABLE];

            $receivedTables[$providerTableId] = $table;

            $providerTableId = $table[self::RESPONSE_TABLE_ID];
            $providerGameId  = $this->_getProviderGameId($providerTableId);

            if ($providerGameId !== null) {
                $condition = ['>',  self::FIELD_TABLE_ID, 0];
                $gameInfo  = $this->_getGame($providerTableId, $providerGameId, $condition);
                if ($gameInfo !== null) {
                    $internalGameId = $gameInfo[self::ID];
                    $tableActive    = $table[self::RESPONSE_OPEN] === 'True';
                    $dealerName     = $this->_getDealerName($table);
                    $dealerId       = $this->_getDealerId($table);
                    $dealerImage    = $this->_getDealerImage($table);

                    if (array_key_exists($providerTableId, $currentTables)) {
                        $updatedTables[$providerTableId] = $table;

                        $tableModel = $tableRepository->update(
                            $provider,
                            $providerTableId,
                            $providerGameId,
                            $internalGameId,
                            $tableActive,
                            $dealerId,
                            $dealerName,
                            $dealerImage
                        );
                    } else {
                        $addedTables[$providerTableId] = $table;

                        $tableModel = $tableRepository->insert(
                            $provider,
                            $providerTableId,
                            $providerGameId,
                            $internalGameId,
                            $tableActive,
                            $dealerId,
                            $dealerName,
                            $dealerImage
                        );
                    }

                    $internalTableId = $tableModel->getId();

                    if (isset($data[self::LIMITS])) {
                        $allLimits = $data[self::LIMITS];
                        foreach ($allLimits as $currency => $limits) {
                            foreach ($limits as $limit) {
                                $providerLimitId = $limit[self::RESPONSE_LIMIT_ID];
                                $limitMin        = $limit[self::RESPONSE_LIMIT_MIN];
                                $limitMax        = $limit[self::RESPONSE_LIMIT_MAX];

                                if (array_key_exists($providerLimitId, $currentLimits)) {
                                    $updatedLimits[$providerLimitId] = $limit;

                                    $limitRepository->update(
                                        $internalTableId,
                                        $providerLimitId,
                                        $currency,
                                        $limitMin,
                                        $limitMax,
                                        $limitMin,
                                        $limitMax,
                                        $limitMin,
                                        $limitMax
                                    );
                                } else {
                                    $addedLimits[$providerLimitId] = $limit;

                                    $limitRepository->insert(
                                        $internalTableId,
                                        $providerLimitId,
                                        $currency,
                                        $limitMin,
                                        $limitMax,
                                        $limitMin,
                                        $limitMax,
                                        $limitMin,
                                        $limitMax
                                    );
                                }
                            }
                        }
                    }

                    continue;
                }
            }

            $unaffectedTables[$providerTableId] = $table;

            if (isset($data[self::LIMITS])) {
                $allLimits = $data[self::LIMITS];
                foreach ($allLimits as $currency => $limits) {
                    foreach ($limits as $limit) {
                        $providerLimitId = $limit[self::RESPONSE_LIMIT_ID];

                        $receivedLimits[$providerLimitId]   = $limit;
                        $unaffectedLimits[$providerLimitId] = $limit;
                    }
                }
            }
        }

        /**
         * @var string           $externalTableId
         * @var CasinoTableModel $table
         */
        foreach ($currentTables as $providerTableId => $table) {
            if (!array_key_exists($providerTableId, $addedTables) &&
                !array_key_exists($providerTableId, $updatedTables)
            ) {
                $deletedTables[$providerTableId] = $table;

                $tableRepository->delete(
                    $table->getProvider(),
                    $table->getExternalTableId()
                );
            }
        }

        /**
         * @var string                $providerLimitId
         * @var CasinoTableLimitModel $limit
         */
        foreach ($currentLimits as $providerLimitId => $limit) {
            if (!array_key_exists($providerLimitId, $addedLimits) &&
                !array_key_exists($providerLimitId, $updatedLimits)
            ) {
                $deletedLimits[$providerLimitId] = $limit;

                $limitRepository->delete(
                    $limit->getCasinoTableId(),
                    $limit->getExternalLimitId()
                );
            }
        }

        $logger = $this->getLogger();

        $logger->info('Update tables result:');
        $logger->info('Received: ', array_keys($receivedTables));
        $logger->info('Added: ', array_keys($addedTables));
        $logger->info('Updated: ', array_keys($updatedTables));
        $logger->info('Deleted: ', array_keys($deletedTables));
        $logger->info('Unaffected: ', array_keys($unaffectedTables));

        $logger->info('Update limits result:');
        $logger->info('Received: ', array_keys($receivedLimits));
        $logger->info('Added: ', array_keys($addedLimits));
        $logger->info('Updated: ', array_keys($updatedLimits));
        $logger->info('Deleted: ', array_keys($deletedLimits));
        $logger->info('Unaffected: ', array_keys($unaffectedLimits));

        return true;
    }

    /**
     * @param string $currencyIso
     * @param string $gameName
     *
     * @return Response
     *
     * @throws InvalidConfigException
     */
    protected function _sendRequest(string $currencyIso, string $gameName): Response
    {
        if (empty($currencyIso)) {
            throw new BadMethodCallException('Currency iso can not be empty');
        }

        $operatorId = $this->_getOperatorId();

        $data = [
            self::REQUEST_GAME_NAME       => $gameName,
            self::REQUEST_OPERATOR_ID     => $operatorId,
            self::REQUEST_PLAYER_CURRENCY => $currencyIso,
        ];

        $url = $this->_getGamesUpdatingUrl();

        $client = $this->getHttpClient();

        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($url)
            ->setData($data)
            ->send();

        return $response;
    }

    /**
     * @param Response $response
     *
     * @return array|false
     *
     * @throws InvalidConfigException
     * @throws Exception
     */
    protected function _validateResponse(Response $response): bool
    {
        if (!$response->getIsOk()) {
            $this->_logResponseError('Response error: ', $response);

            return false;
        }

        $data = $response->getData();

        if (empty($data)) {
            $this->_logResponseError('Response data empty: ', $response);

            return false;
        }

        if (!isset($data[self::RESPONSE_GAME_DATA])) {
            $this->_logResponseError(self::RESPONSE_GAME_DATA . ' not found in response data: ', $response);

            return false;
        }

        if (!isset($data[self::RESPONSE_GAME_DATA][self::RESPONSE_TABLES])) {
            $this->_logResponseError(self::RESPONSE_TABLES . ' not found in game data: ', $response);

            return false;
        }

        return true;
    }

    /**
     * @param array $table
     *
     * @return string
     */
    protected function _getDealerName(array $table): string
    {
        return $table[self::RESPONSE_DEALER_NAME] ?? 'Dealer';
    }

    /**
     * @param array $table
     *
     * @return int
     */
    protected function _getDealerId(array $table): int
    {
        return 0;
    }

    /**
     * @param array $table
     *
     * @return string
     */
    protected function _getDealerImage(array $table): string
    {
        return '';
    }

    /**
     * @param string   $message
     * @param Response $response
     *
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function _logResponseError(string $message, Response $response)
    {
        $logger = $this->getLogger();

        $logger->error(
            $message . print_r(['code' => $response->getStatusCode(), 'content' => $response->getContent()], true)
        );
    }

    #region configs

    /**
     * @return array
     */
    protected function _getGamesName(): array
    {
        return self::GAMES_NAME;
    }

    /**
     * @return string
     *
     * @throws InvalidConfigException
     */
    protected function _getOperatorId(): string
    {
        return (string)$this->_getProviderConfigValue(self::CONFIG_OPERATOR_ID);
    }

    /**
     * @return string
     *
     * @throws InvalidConfigException
     */
    protected function _getGamesUpdatingUrl(): string
    {
        return (string)$this->_getProviderConfigValue(self::CONFIG_GAMES_UPDATING_URL);
    }

    /**
     * @param string $providerTableId
     *
     * @return string|null
     */
    protected function _getProviderGameId(string $providerTableId)
    {
        /**
         * @var string $gameId
         * @var array  $tables
         */
        foreach (self::GAME_TABLES_ID as $gameId => $tables) {
            if (in_array($providerTableId, $tables, true)) {
                return $gameId;
            }
        }

        return null;
    }

    #endregion configs

    #endregion helpers

    #endregion methods
}
