<?php

namespace app\modules\casino\components\commands;

use app\models\casino\CasinoCategory;
use app\models\casino\CasinoGame;
use app\models\casino\CasinoGameCategory;
use app\models\casino\CasinoGameDevice;
use app\modules\casino\components\factories\FactoryLogger;
use app\modules\casino\components\helpers\CacheHelper;
use app\modules\casino\components\helpers\ServiceHelper;
use app\modules\casino\components\ProviderService;
use app\modules\casino\models\Settings;
use Enterra\Casino\Repository\CurrencyInfo\CurrencyInfo;
use Enterra\Casino\Repository\Table\CasinoTableManagerInterface;
use Enterra\Casino\Repository\Table\CasinoTableManagerYii;
use Enterra\Casino\Repository\Table\CasinoTableModel;
use Enterra\Casino\Repository\TableLimit\CasinoTableLimitManagerInterface;
use Enterra\Casino\Repository\TableLimit\CasinoTableLimitManagerYii;
use Enterra\Casino\Repository\TableLimit\CasinoTableLimitModel;
use Exception;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Yii;
use yii\base\BaseObject;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * Class CommandServiceAbstract
 *
 * @package app\modules\casino\components\commands
 */
abstract class CommandServiceAbstract extends Component implements CommandServiceInterface
{
    #region properties

    /**
     * @var string
     */
    protected $_providerName = '';

    /**
     * @var string
     */
    protected $_parentProviderName = '';

    /** @var array */
    protected $_providerConfig;

    /** @var LoggerInterface */
    protected $_logger;

    /** @var CurrencyInfo */
    protected $_currencyInfo;

    /** @var CasinoTableModel[] */
    protected $_tables;

    /** @var CasinoTableLimitModel[] */
    protected $_limits;

    #endregion properties

    #region methods

    #region getters

    /**
     * @return string
     */
    public function getProviderName(): string
    {
        return $this->_providerName;
    }

    /**
     * @return string
     */
    public function getParentProviderName(): string
    {
        return $this->_parentProviderName;
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getProviderConfig(): array
    {
        if ($this->_providerConfig === null) {
            $this->_providerConfig = ProviderService::getConfig($this->getProviderName());
        }

        return $this->_providerConfig;
    }

    /**
     * @return LoggerInterface
     *
     * @throws InvalidConfigException
     */
    public function getLogger(): LoggerInterface
    {
        if ($this->_logger === null) {
            $loggerFactory = new FactoryLogger();

            $options = [
                FactoryLogger::OPTION_OUTPUT    => FactoryLogger::DEFAULT_LOG_PATH .
                    FactoryLogger::CASINO_CRON_LOG_NAME,
                FactoryLogger::OPTION_MIN_LEVEL => Logger::ERROR,
            ];

            $this->_logger = $loggerFactory($options);
        }

        return $this->_logger;
    }

    /**
     * @return CurrencyInfo
     *
     * @throws InvalidConfigException
     */
    public function getCurrencyInfo(): CurrencyInfo
    {
        if ($this->_currencyInfo === null) {
            $currencyInfo = ServiceHelper::getCurrencyInfo();

            $currencyInfo->fields = [];
            $currencyInfo->addField($currencyInfo::FIELD_CURRENCY_ID);
            $currencyInfo->addField($currencyInfo::FIELD_ISO_CODE);
            $currencyInfo->addField($currencyInfo::FIELD_MONEY_TYPE);
            $currencyInfo->addField($currencyInfo::FIELD_USE_FOR_CASINO);
            $currencyInfo->addField($currencyInfo::FIELD_ACTIVE_FLAG);

            $this->_currencyInfo = $currencyInfo;
        }

        return $this->_currencyInfo;
    }

    #endregion getters

    #region setters

    /**
     * @param string $providerName
     */
    public function setProviderName(string $providerName)
    {
        $this->_providerName = $providerName;
    }

    /**
     * @param string $parentProviderName
     */
    public function setParentProviderName(string $parentProviderName)
    {
        $this->_parentProviderName = $parentProviderName;
    }

    /**
     * @param array $providerConfig
     */
    public function setProviderConfig(array $providerConfig)
    {
        $this->_providerConfig = $providerConfig;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->_logger = $logger;
    }

    /**
     * @param CurrencyInfo $currencyInfo
     */
    public function setCurrencyInfo(CurrencyInfo $currencyInfo)
    {
        $this->_currencyInfo = $currencyInfo;
    }

    #endregion setters

    #region main

    /**
     * @return bool
     *
     * @throws InvalidConfigException
     */
    public function updateGames(): bool
    {
        $providerName = $this->getProviderName();

        $logger = $this->getLogger();

        $this->getLogger()->info("Start 'updateGames': providerName = $providerName");

        try {
            if (!ProviderService::isActive($providerName)) {
                $logger->info("Update games not required: provider is not active");

                $result = true;
            } else {
                $result = $this->_updateGames();
            }
        } catch (Exception $e) {
            $logger->error("Update games aborted: exception");
            $logger->error($e->getMessage());
            $logger->error($e->getTraceAsString());

            $result = false;
        }

        if ($result) {
            $logMessage = "End 'updateGames': providerName = $providerName, result = success";
        } else {
            $logMessage = "End 'updateGames': providerName = $providerName, result = failed";
        }

        $this->getLogger()->info($logMessage);

        CacheHelper::deleteIfExist(CacheHelper::CASINO_GAME_LIST);

        return $result;
    }

    #endregion main

    #region helpers

    /**
     * @return bool
     */
    abstract protected function _updateGames(): bool;

    /**
     * @param string $name
     *
     * @return mixed
     *
     * @throws InvalidConfigException
     */
    protected function _getProviderConfigValue(string $name)
    {
        $config = $this->getProviderConfig();
        if (!isset($config[$name])) {
            throw new InvalidConfigException($name . ' not found in provider config');
        }
        if (empty($config[$name])) {
            throw new InvalidConfigException($name . ' can not be empty');
        }

        return $config[$name];
    }

    /**
     * @return CasinoTableModel[]
     *
     * @throws InvalidConfigException
     */
    protected function _getTables(): array
    {
        if ($this->_tables === null) {
            /** @var CasinoTableManagerInterface $tableManager */
            $tableManager = Yii::$app->get(CasinoTableManagerYii::class);

            $tables = [];

            $currentTables = $tableManager->selectAllByOptions(['f_provider' => $this->getProviderName()]);
            if (!empty($currentTables)) {
                foreach ($currentTables as $table) {
                    $tables[$table->getExternalTableId()] = $table;
                }
            }

            $this->_tables = $tables;
        }

        return $this->_tables;
    }

    /**
     * @return CasinoTableLimitModel[]
     *
     * @throws InvalidConfigException
     */
    protected function _getLimits(): array
    {
        if ($this->_limits === null) {
            /** @var CasinoTableLimitManagerInterface $tableLimitManager */
            $tableLimitManager = Yii::$app->get(CasinoTableLimitManagerYii::class);

            $_limits = $tableLimitManager->selectAll();
            if ($_limits === false) {
                $_limits = [];
            }

            $tables  = [];
            $_tables = $this->_getTables();
            foreach ($_tables as $table) {
                $tables[(int)$table->getId()] = $table;
            }

            $limits = [];
            foreach ($_limits as $limit) {
                if (isset($tables[(int)$limit->getCasinoTableId()])) {
                    $limits[$limit->getExternalLimitId()] = $limit;
                }
            }

            $this->_limits = $limits;
        }

        return $this->_limits;
    }

    /**
     * @param array $condition
     * @param bool $all
     * @return array
     *
     * @throws InvalidConfigException
     */
    protected function _getGames(array $condition = null, bool $all = false): array
    {
        $providerCodeCondition = ['=', 'f_provider_code', $this->getProviderName()];
        $runtimeCache          = ServiceHelper::getArrayCacheService();
        $key                   = md5(self::class . '_' . json_encode($providerCodeCondition) . '_' . json_encode($condition));

        if (!$runtimeCache->exists($key)) {
            $gameListMode = Settings::find()->getGameListMode();
            $query        = CasinoGame::find()->where($providerCodeCondition);
            if (!empty($condition)) {
                $query->andWhere($condition);
            }

            $runtimeCache->set($key, $query->gamesForApiGameList($gameListMode, $all));
        }
        return $runtimeCache->get($key);
    }

    /**
     * @param string $providerTableId
     * @param string $providerGameId
     * @param array $condition
     *
     * @return array|null
     *
     * @throws InvalidConfigException
     */
    protected function _getGame(string $providerTableId, string $providerGameId, array $condition = null)
    {
        foreach ($this->_getGames($condition) as $key => $value) {
            if (!isset($value['table']['id'])) {
                throw new InvalidConfigException('table id not found in game config');
            }

            if ((string)$value['table']['id'] === $providerTableId) {
                if (!isset($value['providerGameId'])) {
                    throw new InvalidConfigException('providerGameId not found in game config');
                }

                if ((string)$value['providerGameId'] === $providerGameId) {
                    if (!isset($value['id'])) {
                        throw new InvalidConfigException('id not found in game config');
                    }

                    return $value;
                }
            }
        }
        return null;
    }

    /**
     * @param string $internalGameId
     * @param string $device
     * @return bool
     */
    protected function _addDeviceToGame(string $internalGameId, string $device): bool
    {
        $deviceGame                   = new CasinoGameDevice();
        $deviceGame->f_casino_game_id = $internalGameId;
        $deviceGame->f_device_type    = $device;

        return $deviceGame->save();
    }

    /**
     * @param string $internalGameId
     * @param string $category
     * @return bool
     */
    protected function _addCategoryToGame(string $internalGameId, string $category): bool
    {
        /** @var ActiveRecord $casinoGamesCategory */

        $casinoGamesCategory = CasinoGameCategory::findOne(['f_alias' => $category]);

        if ($casinoGamesCategory === null) {
            $casinoGamesCategory = CasinoGameCategory::findOne(['f_alias' => 'uncategorized' ]);
        }
        $categoryGame                       = new CasinoCategory();
        $categoryGame->f_casino_game_id     = $internalGameId;
        $categoryGame->f_casino_category_id = $casinoGamesCategory->f_id;

        return $categoryGame->save();
    }

    /**
     * @param string $provider
     * @return int
     */
    protected function _getLastOrderNumber(string $provider): int
    {
        /** @var ActiveRecord CasinoGame */
        $maxOrderString = CasinoGame::find()
            ->where(['f_provider_code' => $provider])
            ->orderBy(['f_order' => SORT_DESC])
            ->limit(1)
            ->one();

        if (!$maxOrderString) {
            return -1;
        }
        return $maxOrderString->getAttribute('f_order');
    }

    #endregion helpers

    #endregion methods
}
