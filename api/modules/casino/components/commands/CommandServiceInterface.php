<?php

namespace app\modules\casino\components\commands;

/**
 * Interface CommandServiceInterface
 *
 * @package app\modules\casino\components\commands
 */
interface CommandServiceInterface
{
    #region methods

    #region main

    /**
     * @return bool
     */
    public function updateGames(): bool;

    #endregion main

    #endregion methods
}
