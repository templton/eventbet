<?php

namespace app\modules\casino\components;

use app\common\helpers\ConfigHelper;
use app\common\helpers\ValidatorHelper;
use app\components\ClientService;
use app\models\casino\CasinoProvider;
use app\models\ProviderConfiguration;
use app\modules\casino\components\helpers\CacheHelper;
use Enterra\Casino\Constants\Betsygames\Common as BetsygamesCommon;
use Enterra\Casino\Constants\Outcomebet\Common as OutcomebetConstants;
use Enterra\Casino\Constants\Venum\VenumCommon;
use UnexpectedValueException;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class ProviderService
 *
 * @package app\modules\casino\components
 */
class ProviderService
{
    #region constants

    const SOFTSWISS   = 'softswiss';
    const EZUGI       = 'ezugi';
    const INBET       = 'inbet';
    const GAMEART     = 'gameart';
    const ONETOUCH    = 'onetouch';
    const PLAYPEARLS  = 'playpearls';
    const DIGITAIN    = 'digitain';
    const BETSTARTERS = 'betstarters';
    const VIVO        = 'vivo';
    const MGS         = 'mgs';
    const WAC         = 'wac';
    const PLATIPUS    = 'platipus_vivo';
    const NUCLEUS     = 'nucleus_vivo';
    const TOMHORN     = 'tomhorn_vivo';
    const BETSOFT     = 'betsoft_vivo';
    const REDRAKE     = 'redrake_vivo';
    const SEVENMOJOS  = 'sevenmojos_vivo';
    const LEAP        = 'leap_vivo';
    const ARROWSEDGE  = 'arrowsedge_vivo';
    const BETSYGAMES  = BetsygamesCommon::PROVIDER_NAME;
    const HABANERO    = 'habanero_wac';
    const PRAGMATIC   = 'pragmatic_wac';
    const EVOLUTION   = 'evolution_wac';
    const NETENT      = 'netent_wac';
    const BOOONGO     = 'booongo_wac';
    const SBO         = 'sbo';
    const FRESHDECK   = 'freshdeck';
    const CALETA      = 'caleta';

    const PROVIDERS = [
        self::SOFTSWISS,
        self::EZUGI,
        self::INBET,
        self::GAMEART,
        self::ONETOUCH,
        self::PLAYPEARLS,
        self::DIGITAIN,
        self::BETSTARTERS,
        self::VIVO,
        self::MGS,
        self::WAC,
        self::PLATIPUS,
        self::NUCLEUS,
        self::TOMHORN,
        self::BETSOFT,
        self::REDRAKE,
        self::SEVENMOJOS,
        self::LEAP,
        self::ARROWSEDGE,
        self::BETSYGAMES,
        self::HABANERO,
        self::PRAGMATIC,
        self::EVOLUTION,
        self::NETENT,
        self::BOOONGO,
        OutcomebetConstants::PROVIDER_NAME,
        OutcomebetConstants::PROVIDER_AMATIC,
        OutcomebetConstants::PROVIDER_APOLLO,
        OutcomebetConstants::PROVIDER_ARISTOCRAT,
        OutcomebetConstants::PROVIDER_AUSTRIA,
        OutcomebetConstants::PROVIDER_BOOONGO,
        OutcomebetConstants::PROVIDER_EGT,
        OutcomebetConstants::PROVIDER_FISHING,
        OutcomebetConstants::PROVIDER_IGROSOFT,
        OutcomebetConstants::PROVIDER_IGT,
        OutcomebetConstants::PROVIDER_KAJOT,
        OutcomebetConstants::PROVIDER_KONAMI,
        OutcomebetConstants::PROVIDER_MASKOT,
        OutcomebetConstants::PROVIDER_MERKUR,
        OutcomebetConstants::PROVIDER_MICROGAMING,
        OutcomebetConstants::PROVIDER_NETENT,
        OutcomebetConstants::PROVIDER_PLAYSON,
        OutcomebetConstants::PROVIDER_PLAYTECH,
        OutcomebetConstants::PROVIDER_PRAGMATIC,
        OutcomebetConstants::PROVIDER_QUICKSPIN,
        OutcomebetConstants::PROVIDER_SPADEGAMING,
        OutcomebetConstants::PROVIDER_WAZDAN,
        OutcomebetConstants::PROVIDER_WMG,
        OutcomebetConstants::PROVIDER_NOVOMATIC,

        VenumCommon::PROVIDER_VENUM,
        VenumCommon::PROVIDER_EGT_JACKPOT,
        VenumCommon::PROVIDER_NOVOMATIC,
        VenumCommon::PROVIDER_PLAYNGO,
        VenumCommon::PROVIDER_PRAGMATIC_PLAY,
        VenumCommon::PROVIDER_AMATIC,
        VenumCommon::PROVIDER_NETENT,
        VenumCommon::PROVIDER_QUICKSPIN,
        VenumCommon::PROVIDER_WAZDAN,

        self::SBO,
        self::FRESHDECK,
        self::CALETA,
    ];

    const SPORTSBOOK_PROVIDERS = [
        self::DIGITAIN,
        self::SBO,
    ];

    const PROVIDERS_HAVING_COMMANDS = [
        self::EZUGI,
        self::VIVO,
        self::PLAYPEARLS,
        self::CALETA,
        OutcomebetConstants::PROVIDER_NAME,
        OutcomebetConstants::PROVIDER_AMATIC,
        OutcomebetConstants::PROVIDER_APOLLO,
        OutcomebetConstants::PROVIDER_ARISTOCRAT,
        OutcomebetConstants::PROVIDER_AUSTRIA,
        OutcomebetConstants::PROVIDER_BOOONGO,
        OutcomebetConstants::PROVIDER_EGT,
        OutcomebetConstants::PROVIDER_FISHING,
        OutcomebetConstants::PROVIDER_IGROSOFT,
        OutcomebetConstants::PROVIDER_IGT,
        OutcomebetConstants::PROVIDER_KAJOT,
        OutcomebetConstants::PROVIDER_KONAMI,
        OutcomebetConstants::PROVIDER_MASKOT,
        OutcomebetConstants::PROVIDER_MERKUR,
        OutcomebetConstants::PROVIDER_MICROGAMING,
        OutcomebetConstants::PROVIDER_NETENT,
        OutcomebetConstants::PROVIDER_PLAYSON,
        OutcomebetConstants::PROVIDER_PLAYTECH,
        OutcomebetConstants::PROVIDER_PRAGMATIC,
        OutcomebetConstants::PROVIDER_QUICKSPIN,
        OutcomebetConstants::PROVIDER_SPADEGAMING,
        OutcomebetConstants::PROVIDER_WAZDAN,
        OutcomebetConstants::PROVIDER_WMG,
        OutcomebetConstants::PROVIDER_NOVOMATIC,
    ];

    #endregion constants

    #region properties

    /** @var array */
    protected static $_config = [];

    #endregion properties

    #region methods

    #region main

    /**
     * @return array
     */
    public static function getClientConfigs(): array
    {
        $configs = [];

        $configHelper = ConfigHelper::getInstance();

        $clients       = $configHelper->getClients();
        $clientsConfig = $configHelper->getClientsConfig();

        foreach ($clients as $clientId) {
            if (isset($clientsConfig[$clientId][ClientService::APP_CONFIG])) {
                $config = $clientsConfig[$clientId][ClientService::APP_CONFIG];

                if (isset($config[ClientService::IDENTITY_PROVIDER], $config[ClientService::USE_ID])) {
                    $identityProvider = $config[ClientService::IDENTITY_PROVIDER];
                    $useId            = $config[ClientService::USE_ID];

                    if (!empty($identityProvider) && $useId === 'external') {
                        $config[ClientService::CLIENT_ID] = $clientId;
                        $configs[$identityProvider]       = $config;
                    }
                }
            }
        }

        return $configs;
    }

    /**
     * @param bool|null $isActive
     *
     * @return array
     * @throws InvalidConfigException
     */
    public static function getProviders($isActive = null): array
    {
        if ($isActive === null) {
            /**
             * Since we can not add/remove providers from webadmin app interface
             * following will work correctly
             */
            return self::PROVIDERS;
        }

        $schema = Yii::$app->db->getTableSchema(
            CasinoProvider::tableName()
        );

        if (null === $schema) {
            return [];
        }

        return CacheHelper::getOrSet(
            CacheHelper::ALL_PROVIDERS
                . ConfigHelper::getInstance()->getSkinKey(),
            static function () use ($isActive) {
                return ArrayHelper::getColumn(
                    CasinoProvider::getProviders($isActive),
                    CasinoProvider::FIELD_CODE
                );
            }
        );
    }

    /**
     * @param string $providerName
     *
     * @return bool
     * @throws
     */
    public static function isActive(string $providerName): bool
    {
        if (!self::_validateProviderName($providerName)) {
            throw new UnexpectedValueException("Provider '$providerName' not found");
        }

        return CacheHelper::getOrSet(
            CacheHelper::ALL_PROVIDERS
                . $providerName
                . ConfigHelper::getInstance()->getSkinKey(),
            static function () use ($providerName) {
                return self::_isActive($providerName);
            });
    }

    /**
     * @param string $providerName
     *
     * @return array
     * @throws InvalidConfigException
     */
    public static function getConfig(string $providerName): array
    {
        if (!self::_validateProviderName($providerName)) {
            throw new UnexpectedValueException("Provider '$providerName' not found");
        }

        //return self::_getConfig($providerName);

        $config = CacheHelper::getOrSet(
            CacheHelper::PROVIDER_CONFIG
                . $providerName
                . ConfigHelper::getInstance()->getSkinKey(),
            static function () use ($providerName) {
                return self::_getConfig($providerName);
            }
        );

        if (!isset($config['singleCurrency'])) {
            $config['singleCurrency'] = 0;
        } else {
            $config['singleCurrency'] = (int)$config['singleCurrency'];
            if ($config['singleCurrency']) {
                if (!isset($config['defaultCurrency'])) {
                    throw new UnexpectedValueException("defaultCurrency not found in provider config");
                }

                $config['defaultCurrency'] = reset($config['defaultCurrency']);
            }
        }

        return $config;
    }

    #endregion main

    #region helpers

    /**
     * @param string $providerName
     *
     * @return array
     */
    protected static function _getConfig(string $providerName): array
    {
        if (!self::_validateProviderName($providerName)) {
            throw new UnexpectedValueException("Provider '$providerName' not found");
        }

        if (empty(self::$_config)) {
            $configuration = ProviderConfiguration::getAll();

            /** @var ProviderConfiguration $entry */
            foreach ($configuration as $entry) {
                self::_assignArrayByPath(self::$_config, $entry);
            }
        }

        return self::$_config[$providerName] ?? [];
    }

    /**
     * @param string $providerName
     *
     * @return bool
     */
    protected static function _isActive(string $providerName): bool
    {
        if (!self::_validateProviderName($providerName)) {
            throw new UnexpectedValueException("Provider '$providerName' not found");
        }
        
//        if ($providerName == 'freshdeck'){
//            return true;
//        }

        //echo "<pre>";print_r($providerName);echo "</pre>";die;

        //echo "<pre>";print_r(CasinoProvider::tableName());echo "</pre>";die;

        return CasinoProvider::find()
            ->where([CasinoProvider::FIELD_CODE => $providerName])
            ->active(true)
            ->exists();
    }

    /**
     * @param string $providerName
     *
     * @return bool
     */
    protected static function _validateProviderName(string $providerName): bool
    {
        /**
         * Since we can not add/remove providers from webadmin app interface
         * following will work correctly
         */
        $validator = ValidatorHelper::getRangeValidator(self::PROVIDERS);

        return $validator->validate($providerName);
    }

    /**
     * @param array                 &$arr
     * @param ProviderConfiguration  $entry
     * @param string                 $separator
     */
    private static function _assignArrayByPath(
        array &$arr,
        ProviderConfiguration $entry,
        string $separator = '.'
    ) {
        $path = str_replace(ProviderConfiguration::PREFIX, '', $entry->f_name);
        $keys = explode($separator, $path);

        if (end($keys) !== 'value') {
            return;
        }

        array_pop($keys);

        foreach ($keys as $key) {
            $arr = &$arr[$key];
        }

        $arr = $entry->f_type === 'array'
            ? json_decode($entry->f_value)
            : $entry->f_value;
    }

    #endregion helpers

    #endregion methods
}
