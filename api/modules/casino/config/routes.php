<?php

$casinoCallbackIndex     = 'casino/callback/index';
$casinoVivoCallbackIndex = 'casino/vivo-callback/index';
$casinoWacCallbackIndex  = 'casino/wac-callback/index';
$casinoOutcomebetCallbackIndex  = 'casino/outcomebet-callback/index';
$casinoVenumCallbackIndex  = 'casino/venum-callback/index';

$casinoDefaultOptions    = 'casino/default/options';
$casinoAggregatorOptions = 'casino/aggregator/option';

return [
    #region default

    #region v1

    'GET     casino/settings'             => 'casino/default/settings',
    'GET     casino/providers'            => 'casino/default/providers',
    'GET     casino/providers-sportsbook' => 'casino/default/providers-sportsbook',
    'GET     casino/category-list'        => 'casino/default/category-list',
    'GET     casino/game-list'            => 'casino/default/game-list',
    'GET     casino/sportsbook-game-list' => 'casino/default/sportsbook-game-list',

    'POST    casino/games/sportsbook/open-real-game' => 'casino/default/sportsbook-open-real-game',
    'POST    casino/open-real-game'                  => 'casino/default/open-real-game',
    'POST    casino/open-demo-game'                  => 'casino/default/open-demo-game',

    'OPTIONS casino/games/sportsbook/open-real-game' => $casinoDefaultOptions,
    'OPTIONS casino/open-real-game'                  => $casinoDefaultOptions,
    'OPTIONS casino/open-demo-game'                  => $casinoDefaultOptions,

    #endregion v1

    #region v2

    'GET     v2/casino/settings'             => 'casino/default/settings',
    'GET     v2/casino/providers'            => 'casino/default/providers',
    'GET     v2/casino/providers-sportsbook' => 'casino/default/providers-sportsbook',
    'GET     v2/casino/categories'           => 'casino/default/category-list',
    'GET     v2/casino/games'                => 'casino/default/game-list',
    'GET     v2/casino/sportsbook-games'     => 'casino/default/sportsbook-game-list',

    'POST    v2/casino/games/sportsbook/session'                  => 'casino/default/sportsbook-open-real-game',
    'POST    v2/casino/games/<gameId:[\w\.\-\_\:]+>/session'      => 'casino/default/open-real-game',
    'POST    v2/casino/games/<gameId:[\w\.\-\_\:]+>/session-demo' => 'casino/default/open-demo-game',

    'OPTIONS    v2/casino/games/sportsbook/session'                  => $casinoDefaultOptions,
    'OPTIONS    v2/casino/games/<gameId:[\w\.\-\_\:]+>/session'      => $casinoDefaultOptions,
    'OPTIONS    v2/casino/games/<gameId:[\w\.\-\_\:]+>/session-demo' => $casinoDefaultOptions,

    #endregion v2

    #endregion default

    #region aggregator

    #region v1

    'GET     v2/casino-provider/games' => 'casino/aggregator/games',

    'POST    v2/casino-provider/games/sportsbook/session'                  => 'casino/aggregator/sportsbook-play',
    'POST    v2/casino-provider/games/<gameId:[\w\.\-\_\:]+>/session'      => 'casino/aggregator/play',
    'POST    v2/casino-provider/games/<gameId:[\w\.\-\_\:]+>/session-demo' => 'casino/aggregator/demo',

    'OPTIONS    v2/casino-provider/games/sportsbook/session'                  => $casinoAggregatorOptions,
    'OPTIONS    v2/casino-provider/games/<gameId:[\w\.\-\_\:]+>/session'      => $casinoAggregatorOptions,
    'OPTIONS    v2/casino-provider/games/<gameId:[\w\.\-\_\:]+>/session-demo' => $casinoAggregatorOptions,

    #endregion v1

    #region v2

    #endregion v2

    #endregion aggregator

    #region provider games

    'games/digitain'         => 'casino/provider-games/digitain',
    'games/digitain-asian'   => 'casino/provider-games/digitain-asian',
    'games/digitain-african' => 'casino/provider-games/digitain-african',
    'games/inbet/game.php'   => 'casino/provider-games/inbet',

    #endregion provider games

    #region callbacks

    [
        'pattern'  => 'play',
        'route'    => $casinoCallbackIndex,
        'defaults' => ['provider' => 'softswiss', 'act' => 'play'],
    ],
    [
        'pattern'  => 'rollback',
        'route'    => $casinoCallbackIndex,
        'defaults' => ['provider' => 'softswiss', 'act' => 'rollback'],
    ],
    [
        'pattern'  => 'sportsbook/<act:[\w\-]+>',
        'route'    => $casinoCallbackIndex,
        'defaults' => ['provider' => 'digitain'],
    ],
    [
        'pattern'  => 'playpearl/<entity:[\w\-]+>/<act:[\w\-]+>',
        'route'    => $casinoCallbackIndex,
        'defaults' => ['provider' => 'playpearls'],
    ],

    '<provider:(sbo)>/<act:[\w\-]+>'                         => $casinoCallbackIndex,

    '<provider:(softswiss)>/<act:[\w\-]+>'                   => $casinoCallbackIndex,
    '<provider:(ezugi)>/<act:[\w\-]+>'                       => $casinoCallbackIndex,
    '<provider:(inbet)>/<act:[\w\-]+>'                       => $casinoCallbackIndex,
    '<provider:(gameart)>/<act:[\w\-]+>'                     => $casinoCallbackIndex,
    '<provider:(onetouch)>/<entity:[\w\-]+>/<act:[\w\-]+>'   => $casinoCallbackIndex,
    '<provider:(playpearls)>/<entity:[\w\-]+>/<act:[\w\-]+>' => $casinoCallbackIndex,
    '<provider:(betstarters)>/<act:[\w\-]+>'                 => $casinoCallbackIndex,
    '<provider:(mgs)>/<act:[\w\-]+>'                         => $casinoCallbackIndex,

    '<provider:(sportsbook)>/<act:[\w\-]+>'                  => $casinoCallbackIndex,
    '<provider:(betsygames)>/<entity:[\w\-]+>/<act:[\w\-]+>' => $casinoCallbackIndex,
    '<provider:(caleta)>/wallet/<act:[\w\-\.]+>'             => $casinoCallbackIndex,

    '<provider:(vivo)>/<act:[\w\-]+>'              => $casinoVivoCallbackIndex,
    '<provider:(platipus_vivo)>/<act:[\w\-\.]+>'   => $casinoVivoCallbackIndex,
    '<provider:(nucleus_vivo)>/<act:[\w\-\.]+>'    => $casinoVivoCallbackIndex,
    '<provider:(tomhorn_vivo)>/<act:[\w\-\.]+>'    => $casinoVivoCallbackIndex,
    '<provider:(betsoft_vivo)>/<act:[\w\-\.]+>'    => $casinoVivoCallbackIndex,
    '<provider:(redrake_vivo)>/<act:[\w\-\.]+>'    => $casinoVivoCallbackIndex,
    '<provider:(7mojos_vivo)>/<act:[\w\-\.]+>'     => $casinoVivoCallbackIndex,
    '<provider:(leap_vivo)>/<act:[\w\-\.]+>'       => $casinoVivoCallbackIndex,
    '<provider:(arrowsedge_vivo)>/<act:[\w\-\.]+>' => $casinoVivoCallbackIndex,

    '<provider:(wac)>/<act:[\w\-]+>'           => $casinoWacCallbackIndex,
    '<provider:(habanero_wac)>/<act:[\w\-]+>'  => $casinoWacCallbackIndex,
    '<provider:(pragmatic_wac)>/<act:[\w\-]+>' => $casinoWacCallbackIndex,
    '<provider:(evolution_wac)>/<act:[\w\-]+>' => $casinoWacCallbackIndex,
    '<provider:(netent_wac)>/<act:[\w\-]+>'    => $casinoWacCallbackIndex,

    '<provider:(outcomebet)>'              => $casinoOutcomebetCallbackIndex,
    '<provider:(venum)>/<act:[\w\-]+>'     => $casinoVenumCallbackIndex,
    '<provider:(freshdeck)>/<act:[\w\-]+>' => $casinoCallbackIndex,

    #endregion callbacks
];

