<?php

use app\modules\casino\components\migrations\ProviderMigration;

/**
 * Class m210823_040128_add_caleta_casino_provider
 */
class m210823_040128_add_caleta_casino_provider extends ProviderMigration
{

    #region constants

    const PROVIDER_NAME = 'caleta';

    #endregion constants

    #region methods

    public function init()
    {
        parent::init();

        $providers = [
            self::PROVIDER_NAME => [
                static::FIELD_NAME      => ucfirst(self::PROVIDER_NAME),
                static::FIELD_IS_ACTIVE => 0,
            ],
        ];

        $this->setProviders($providers);
    }

    #endregion methods
}
