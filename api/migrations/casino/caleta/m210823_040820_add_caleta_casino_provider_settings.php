<?php

use app\models\ProviderConfiguration;
use app\modules\casino\components\migrations\ProviderSettingsMigration;

/**
 * Class m210823_040820_add_caleta_casino_provider_settings
 */
class m210823_040820_add_caleta_casino_provider_settings extends ProviderSettingsMigration
{
    #region constants

    const PREFIX_NAME = ProviderConfiguration::PREFIX . 'caleta' . '.';

    const NAME_LOBBY_URL           = 'lobbyUrl';
    const NAME_DEPOSIT_URL         = 'depositUrl';
    const NAME_THUMBNAIL           = 'thumbnailParam';
    const NAME_OPEN_DEMO_GAME_URL  = 'openDemoGameUrl';
    const NAME_OPEN_GAME_URL       = 'openGameUrl';
    const NAME_UPDATE_GAME_URL     = 'updateGameUrl';
    const NAME_OPEN_GAME_STRATEGY  = 'openGameStrategy';
    const NAME_OPERATOR_ID         = 'operatorId';
    const NAME_PRIVATE_KEY         = 'privateKey';
    const NAME_PUBLIC_KEY          = 'publicKey';

    #endregion constants

    #region methods

    public function init()
    {
        parent::init();

        $editableText   = [
            self::CONFIG_EDITABLE => [
                self::FIELD_TYPE  => self::TYPE_INTEGER,
                self::FIELD_VALUE => '1',
            ],
            self::CONFIG_VALUE    => [
                self::FIELD_TYPE  => self::TYPE_STRING,
                self::FIELD_VALUE => '',
            ],
            self::CONFIG_VISIBLE  => [
                self::FIELD_TYPE  => self::TYPE_INTEGER,
                self::FIELD_VALUE => '1',
            ],
            self::CONFIG_WIDGET   => [
                self::FIELD_TYPE  => self::TYPE_STRING,
                self::FIELD_VALUE => 'text',
            ],
        ];

        $noEditableText = [
            self::CONFIG_EDITABLE => [
                self::FIELD_TYPE  => self::TYPE_INTEGER,
                self::FIELD_VALUE => '0',
            ],
            self::CONFIG_VALUE    => [
                self::FIELD_TYPE  => self::TYPE_STRING,
                self::FIELD_VALUE => '',
            ],
            self::CONFIG_VISIBLE  => [
                self::FIELD_TYPE  => self::TYPE_INTEGER,
                self::FIELD_VALUE => '1',
            ],
            self::CONFIG_WIDGET   => [
                self::FIELD_TYPE  => self::TYPE_STRING,
                self::FIELD_VALUE => 'text',
            ],
        ];

        $editableText[self::CONFIG_VALUE][self::FIELD_VALUE] = 'url_thumb';
        $thumbnail                                           = $editableText;

        $editableText[self::CONFIG_VALUE][self::FIELD_VALUE] = '';
        $lobbyUrl                                            = $editableText;

        $editableText[self::CONFIG_VALUE][self::FIELD_VALUE] = '';
        $depositURL                                         = $editableText;

        $editableText[self::CONFIG_VALUE][self::FIELD_VALUE] = '';
        $openGameUrl                                         = $editableText;

        $editableText[self::CONFIG_VALUE][self::FIELD_VALUE] = '';
        $openDemoGameUrl                                     = $editableText;

        $editableText[self::CONFIG_VALUE][self::FIELD_VALUE] = '';
        $updateGameUrl                                       = $editableText;

        $noEditableText[self::CONFIG_VALUE][self::FIELD_VALUE] = 'detect';
        $openGameStrategy                                      = $noEditableText;

        $editableText[self::CONFIG_VALUE][self::FIELD_VALUE] = '';
        $operatorId                                          = $editableText;

        $editableText[self::CONFIG_VALUE][self::FIELD_VALUE] = '';
        $privateKey                                           = $editableText;

        $editableText[self::CONFIG_VALUE][self::FIELD_VALUE] = '';
        $publicKey                                           = $editableText;

        $this->setSettings([
            self::PREFIX_NAME . self::NAME_OPERATOR_ID          => $operatorId,
            self::PREFIX_NAME . self::NAME_PRIVATE_KEY          => $privateKey,
            self::PREFIX_NAME . self::NAME_PUBLIC_KEY           => $publicKey,
            self::PREFIX_NAME . self::NAME_OPEN_GAME_URL        => $openGameUrl,
            self::PREFIX_NAME . self::NAME_OPEN_DEMO_GAME_URL   => $openDemoGameUrl,
            self::PREFIX_NAME . self::NAME_OPEN_GAME_STRATEGY   => $openGameStrategy,
            self::PREFIX_NAME . self::NAME_UPDATE_GAME_URL      => $updateGameUrl,
            self::PREFIX_NAME . self::NAME_DEPOSIT_URL               => $depositURL,
            self::PREFIX_NAME . self::NAME_LOBBY_URL                 => $lobbyUrl,
            self::PREFIX_NAME . self::NAME_THUMBNAIL                 => $thumbnail,
        ]);
    }
    #endregion methods
}
