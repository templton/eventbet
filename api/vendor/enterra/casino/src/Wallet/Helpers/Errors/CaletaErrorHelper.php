<?php

namespace Enterra\Casino\Wallet\Helpers\Errors;

use Enterra\Casino\Common\Exceptions\CasinoWalletException;
use Enterra\Casino\ErrorCode\CaletaErrorCode;
use Enterra\Casino\Wallet\Models\WalletResultAbstract;
use Enterra\Casino\Wallet\WalletExternal;


class CaletaErrorHelper extends AbstractErrorHelper
{
    /**
     * @param WalletResultAbstract $walletResult
     * @return int
     * @throws CasinoWalletException
     */
    public static function getErrorCode($walletResult): int
    {
        $walletErrorCode = parent::getErrorCode($walletResult);

        $errorCodes = [
            WalletExternal::E_PLAYER_NOT_FOUND              => CaletaErrorCode::USER_IS_NOT_FOUND,
            WalletExternal::E_INSUFFICIENT_FUNDS            => CaletaErrorCode::NOT_ENOUGH_MONEY,
        ];

        return $errorCodes[$walletErrorCode] ?? CaletaErrorCode::SYSTEM_ERROR;
    }
}