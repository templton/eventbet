<?php

namespace Enterra\Casino\Constants\Caleta;

class CaletaConstants
{
    const PROVIDER_NAME                 = 'caleta';

    const AUTH_HEADER_NAME = 'x-auth-signature';
    const PARAM_SIGNATURE = 'xSignature';
    const PARAM_BODY = 'rowBody';

    const CONFIG_FIELD_OPERATOR_ID      = 'operatorId';
    const CONFIG_FIELD_PRIVATE_KEY      = 'privateKey';
    const CONFIG_FIELD_PUBLIC_KEY       = 'publicKey';
    const CONFIG_OPEN_GAME_STRATEGY     = 'openGameStrategy';
    const CONFIG_OPEN_DEMO_GAME_URL     = 'openDemoGameUrl';
    const CONFIG_OPEN_GAME_URL          = 'openGameUrl';
    const CONFIG_FIELD_UPDATE_GAME_URL  = 'updateGameUrl';
    const CONFIG_LOBBY_URL              = 'lobbyUrl';
    const CONFIG_DEPOSIT_URL            = 'depositUrl';
    const CONFIG_FIELD_THUMB_NAME       = 'thumbnailParam';
    const CURRENCY_DEMO                 = 'FUN';

    const TRANSACTION_TYPE_BET          = 'bet';
    const TRANSACTION_TYPE_WIN          = 'win';
    const TRANSACTION_TYPE_ROLLBACK     = 'rollback';
    const TRANSACTION_TYPE_CREDIT       = 'credit';
    const TRANSACTION_TYPE_DEBIT        = 'debit';

    const REQUEST_HEADER_SIGN                   = 'x-auth-signature';
    const REQUEST_UUID                          = 'request_uuid';
    const REQUEST_TOKEN                         = 'token';
    const REQUEST_TRANSACTION_UUID              = 'transaction_uuid';
    const REQUEST_REFERENCE_TRANSACTION_UUID    = 'reference_transaction_uuid';
    const REQUEST_REFENDED_TRANSACTION_UUID     = 'reference_transaction_uuid';
    const REQUEST_ROUND                         = 'round';
    const REQUEST_GAME_ID                       = 'game_id';
    const REQUEST_GAME_CODE                     = 'game_code';
    const REQUEST_CURRENCY                      = 'currency';
    const REQUEST_AMOUNT                        = 'amount';
    const REQUEST_BET                           = 'bet';

    const RESPONSE_USER                         = 'user';
    const RESPONSE_STATUS                       = 'status';
    const RESPONSE_CURRENCY                     = 'currency';
    const RESPONSE_BALANCE                      = 'balance';

    const MONEY_MULTIPLE = 1000;
}