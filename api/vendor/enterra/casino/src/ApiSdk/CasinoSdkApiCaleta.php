<?php

namespace Enterra\Casino\ApiSdk;

use app\models\casino\CasinoGame;
use app\modules\casino\components\ProviderService;
use Enterra\Casino\ApiSdk\CasinoSdkApiAbstract;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use http\Exception\RuntimeException;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Class CasinoSdkApiCaleta
 *
 * @package Enterra\Casino\ApiSdk
 */
class CasinoSdkApiCaleta extends CasinoSdkApiAbstract
{
    /**
     * Метод вызывается при запуске демо игры
     */
    public function openDemoGame($gameId, $clientIp, $locale = null, $deviceType = 'desktop', $limitId = null): array
    {
        $token = '';
        $user = '';

        $data = $this->_createBodyOpenGame($gameId, $token, $user,CaletaConstants::CURRENCY_DEMO,'en','en','');

        $url = $this->_getOpenDemoGameUrl();
        $response = $this->_sendRequest($data, $url);

        if (!$this->validateResponse($response) || !isset($response->data['url'])){
            throw new RuntimeException('Can not get open game url');
        }

        $strategy = $this->getValueConfigParam(CaletaConstants::CONFIG_OPEN_GAME_STRATEGY);

        return [
            'launch_options' => [
                'game-url' => $response->data['url'],
                'strategy' => $strategy,
            ],
        ];
    }

    /**
     * Метод вызывается при запуске реальной игры
     */
    public function openRealGame(
        $gameId,
        $moneyType,
        $clientIp,
        $userId,
        $currency = null,
        $locale = null,
        $balance = null,
        $deviceType = 'desktop',
        $limitId = null
    ): array {
        $currencyInfo   = $this->getCurrencyInfo();
        if (!$currency) {
            $currency = $currencyInfo->getIsoCodeByMoneyType($moneyType);
        }

        $gameManager    = $this->getGameManager();
        $userManager    = $this->getUserManager();
        $locale         = $locale ?? 'en';

        $user           = $this->getUserManager()->selectOneByUserId($userId);
        $country        = $user->getCountryId();
        $country        = $country === 0 ? 'US' : $country;

        if (!$currencyInfo || !$gameManager || !$userManager) {
            throw new InvalidArgumentException();
        }

        list($session, $clientSessionId) = $this->_prepareSession($userId, $gameId, $locale, $currency);

        if (!$session) {
            throw new RuntimeException('Could not write session data to the database', 500);
        }

        $sessionId = $session->getSessionId();

        $game = $this->getGameManager()->insert(
            $session->getId(),
            $sessionId,
            0
        );

        if (!$game) {
            throw new Exception('Insert or update game failed', 500);
        }

        $strategy = $this->getValueConfigParam(CaletaConstants::CONFIG_OPEN_GAME_STRATEGY);

        $data = $this->_createBodyOpenGame($gameId, $sessionId, $userId, $currency, $country, $locale, '');
        $url = $this->_getOpenRealGameUrl();

        $gameInfo = $this->_sendRequest($data, $url);

        if (!$this->validateResponse($gameInfo) || !isset($gameInfo->data['url'])){
            throw new RuntimeException('Can not get open game url');
        }

        return [
            'launch_options'  => [
                'game-url' => $gameInfo->data['url'],
                'strategy' => $strategy,
            ],
            'session_id'      => $sessionId,
            'clientSessionId' => $clientSessionId,
        ];
    }

    private function _prepareSession($userId, $gameId, $locale, $currency)
    {
        $sessionManager = $this->getSessionManager();

        if (!$sessionManager) {
            throw new InvalidArgumentException();
        }

        $providerGameId = $this->getProviderGameIdFromGameId($gameId);
        $providerName    = $this->getProviderNameFromGameId($gameId);

        $sessionId = $this->_generateSessionId(
            $userId,
            $providerGameId,
            $providerName
        );

        $clientSessionId = $this->generateClientSessionId(
            $userId,
            $providerGameId,
            $providerName,
            $sessionId
        );

        $session = $sessionManager->insert(
            $userId,
            $sessionId,
            $providerName,
            $gameId,
            $providerGameId,
            $locale,
            $currency,
            '',
            null,
            $clientSessionId
        );

        return [$session, $clientSessionId];
    }

    private function validateApiResponse(Response $response)
    {
        if (!$response->isOk || !is_array($response->data) || empty($response->data)){
            return false;
        }

        if (!isset($response['data']['url'])){
            return false;
        }

        return true;
    }

    protected function _generateSessionId(string $userId, string $providerGameId, string $providerName): string
    {
        $date         = new \DateTime();
        $paramsString = $userId . $providerGameId . $providerName . $date->getTimestamp();

        return substr(md5($paramsString), 0, 32);
    }

    private function _getOpenDemoGameUrl()
    {
        return $this->getValueConfigParam(CaletaConstants::CONFIG_OPEN_DEMO_GAME_URL);
    }

    private function _getOpenRealGameUrl()
    {
        return $this->getValueConfigParam(CaletaConstants::CONFIG_OPEN_GAME_URL);
    }

    /**
     * @param int $gameId Идентификатор игры по нумерации evenbet
     * @param string $token
     * @param string $user
     * @return array
     */
    private function _createBodyOpenGame(int $gameId, string $token, string $user, string $currency, string $country, string $lang, string $subPartnerId)
    {
        $operatorId = $this->_getOperatorId();

        $lobbyUrl = $this->_getLobbyUrl();
        $depositUrl = $this->_getLobbyUrl();
        $gameCode = $this->getProviderGameIdFromGameId($gameId);

        return [
            "user" => $user,
            "token" => $token,
            "sub_partner_id" => $subPartnerId,
            "operator_id" => $operatorId,
            "lobby_url" => $lobbyUrl,
            "lang" => $lang,
            "game_code" => $gameCode,
            "deposit_url" => $depositUrl,
            "currency" => $currency,
            "country" => $country
        ];
    }

    private function _sendRequest($data, $url): Response
    {
        $signature = $this->createSignature($data);

        $response = (new Client())->createRequest()
            ->setMethod('POST')
            ->setFormat(Client::FORMAT_JSON)
            ->setHeaders([
                'X-Auth-Signature' => $signature,
                'Content-Type' => 'application/json'
            ])
            ->setUrl($url)
            ->setData($data)
            ->send();

        if (!$this->validateResponse($response)){
            throw new \RuntimeException("Error sending request to " . ProviderService::CALETA . '; ' . print_r($response->getData(), true));
        }

        return $response;
    }

    private function validateResponse(Response $response): bool
    {
        return !(!$response->isOk || !is_array($response->data) || empty($response->data));
    }

    private function _getDepositUrl(): string
    {
        return $this->getValueConfigParam(CaletaConstants::CONFIG_DEPOSIT_URL);
    }

    private function _getLobbyUrl(): string
    {
        return $this->getValueConfigParam(CaletaConstants::CONFIG_LOBBY_URL);
    }

    private function createSignature(array $data): string
    {
        $data = json_encode($data);

        $private_key = $this->_getPrivateKey();
        $signature = "";
        $algo = "SHA256";
        openssl_sign($data, $signature, $private_key, $algo);

        return base64_encode($signature);
    }

    private function _getPrivateKey()
    {
        $fileKey = $this->getValueConfigParam(CaletaConstants::CONFIG_FIELD_PRIVATE_KEY);
        $fileKey = preg_replace('@(?<!(GIN|RSA|ATE|KEY|END|LIC))\s@', "\n", $fileKey);
        return openssl_pkey_get_private($fileKey);
    }

    private function _getOperatorId(): string
    {
        return $this->getValueConfigParam(CaletaConstants::CONFIG_FIELD_OPERATOR_ID);
    }

}