<?php

namespace Enterra\Casino\CallbackSdk;

use Enterra\Casino\Common\Exceptions\CasinoWalletException;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use Enterra\Casino\ErrorCode\CaletaErrorCode;
use Enterra\Casino\Repository\GameTransaction\CasinoGameTransactionManagerInterface;
use Enterra\Casino\Repository\GameTransaction\CasinoGameTransactionModel;
use Enterra\Casino\Repository\Session\CasinoSessionModel;
use Enterra\Casino\Request\Caleta\AbstractRequest;
use Enterra\Casino\Request\Caleta\AbstractTransactionRequest;
use Enterra\Casino\Request\Caleta\BalanceRequest;
use Enterra\Casino\Request\Caleta\BetRequest;
use Enterra\Casino\Request\Caleta\CheckRequest;
use Enterra\Casino\Request\Caleta\RollbackRequest;
use Enterra\Casino\Request\Caleta\WinRequest;
use Enterra\Casino\Response\Caleta\AbstractResponse;
use Enterra\Casino\Response\Caleta\BalanceResponse;
use Enterra\Casino\Response\Caleta\BetResponse;
use Enterra\Casino\Response\Caleta\CheckResponse;
use Enterra\Casino\Response\Caleta\ErrorResponse;
use Enterra\Casino\Response\Caleta\RollbackResponse;
use Enterra\Casino\Response\Caleta\WinResponse;
use Enterra\Casino\Wallet\Models\CreditResult;
use Enterra\Casino\Wallet\Models\DebitResult;
use Enterra\Casino\Wallet\Models\WalletResultAbstract;
use Enterra\Casino\Wallet\WalletInterface;
use LogicException;
use UnexpectedValueException;
use Enterra\Casino\Wallet\Helpers\Errors\CaletaErrorHelper;

/**
 * Class CasinoSdkCallbackCaleta
 *
 * @package Enterra\Casino\CallbackSdk
 */
class CasinoSdkCallbackCaleta extends CasinoSdkCallbackAbstract
{

    const METHOD_CHECK = 'check';
    const METHOD_BALANCE = 'balance';
    const METHOD_BET = 'bet';
    const METHOD_WIN = 'win';
    const METHOD_ROLLBACK = 'rollback';

    const METHODS = [
        self::METHOD_CHECK,
        self::METHOD_BALANCE,
        self::METHOD_BET,
        self::METHOD_WIN,
        self::METHOD_ROLLBACK
    ];

    /**
     * @var CasinoSessionModel
     */
    protected $_session;

    protected $_gameSession;

    public function getMethods(): array
    {
        return self::METHODS;
    }

    #region methods

    public function check(CheckRequest $request)
    {
        /** @var ErrorResponse $response */
        $this->_validateRequest($request, $response);

        if (null !== $response) {
            return $response;
        }

        $session = $this->_prepareSession($request->getSessionId());

        if ($session === null) {
            $response = new ErrorResponse($request);
            $response->setStatus(CaletaErrorCode::STATUS_RS_ERROR_INVALID_TOKEN);

            return $response;
        }

        $response = new CheckResponse($request);
        $response->setToken($session->getSessionId());

        return $response;
    }

    public function balance(BalanceRequest $request)
    {
        /** @var ErrorResponse $response */
        $this->_validateRequest($request, $response);

        if (null !== $response) {
            return $response;
        }

        $session = $this->_session;

        $walletResult = $this->_balance($session);

        if (!$this->_validateWalletResult($walletResult)){
            $innerErrorCode = CaletaErrorHelper::getErrorCode($walletResult);
            $response = new ErrorResponse();
            $response->setMappedInnerErrorToProviderStatus($innerErrorCode);
            return $response;
        }

        $balance = $walletResult->getBalance();

        $response = new BalanceResponse($request);
        $response->setBalance($balance);

        return $this->_prepareResponse($response, $request);
    }

    public function bet(BetRequest $request)
    {
        /** @var ErrorResponse $response */
        $this->_validateRequest($request, $response);

        if (null !== $response) {
            return $response;
        }

        $moneyConverter = $this->getMoneyConverter();
        $session = $this->_session;
        $walletResult = $this->_balance($session);

        if (!$this->_validateWalletResult($walletResult)){
            $innerErrorCode = CaletaErrorHelper::getErrorCode($walletResult);
            $response = new ErrorResponse();
            $response->setMappedInnerErrorToProviderStatus($innerErrorCode);
            return $response;
        }

        $balance = $walletResult->getBalance();

        $currency = $session->getCurrency();
        $amountInCents = $request->getAmount();
        $extAmount = $moneyConverter->convert($amountInCents, $currency);

        if ($extAmount > $balance){
            $response = new ErrorResponse($request);
            $response->setStatus(CaletaErrorCode::STATUS_RS_ERROR_NOT_ENOUGH_MONEY);
            return $response;
        }

        try {
            $this->_executeTransactionRequest($request, CaletaConstants::TRANSACTION_TYPE_BET);
        } catch (\Throwable $e){
            $this->addError('ERROR', __METHOD__, $e->getMessage());

            $response = new ErrorResponse($request);
            $response->setErrorMessage($e->getMessage());
            $response->setStatus(CaletaErrorCode::STATUS_RS_ERROR_UNKNOWN);

            return $response;
        }

        $walletResult = $this->_balance($this->_session);

        if (!$this->_validateWalletResult($walletResult)){
            $innerErrorCode = CaletaErrorHelper::getErrorCode($walletResult);
            $response = new ErrorResponse();
            $response->setMappedInnerErrorToProviderStatus($innerErrorCode);
            return $response;
        }

        $balance = $walletResult->getBalance();

        $response = new BetResponse($request);
        $response->setBalance($balance);

        return $this->_prepareResponse(
            $response,
            $request
        );
    }

    public function win(WinRequest $request)
    {
        /** @var ErrorResponse $response */
        $this->_validateRequest($request, $response);

        if (null !== $response) {
            return $response;
        }

        try{
            $this->_executeTransactionRequest($request, CaletaConstants::TRANSACTION_TYPE_WIN);
        }catch (\Throwable $e){
            $this->addError('ERROR', __METHOD__, $e->getMessage());

            $response = new ErrorResponse($request);
            $response->setErrorMessage($e->getMessage());
            $response->setStatus(CaletaErrorCode::STATUS_RS_ERROR_UNKNOWN);

            return $response;
        }

        $walletResult = $this->_balance($this->_session);

        if (!$this->_validateWalletResult($walletResult)){
            $innerErrorCode = CaletaErrorHelper::getErrorCode($walletResult);
            $response = new ErrorResponse();
            $response->setMappedInnerErrorToProviderStatus($innerErrorCode);
            return $response;
        }

        $balance = $walletResult->getBalance();

        $response = new WinResponse($request);
        $response->setBalance($balance);

        return $this->_prepareResponse(
            $response,
            $request
        );
    }

    public function rollback(RollbackRequest $request)
    {
        /** @var ErrorResponse $response */
        $this->_validateRequest($request, $response);

        if (null !== $response) {
            return $response;
        }

        try{
            $this->_executeTransactionRequest($request, CaletaConstants::TRANSACTION_TYPE_ROLLBACK);
        }catch (\Throwable $e){
            $this->addError('ERROR', __METHOD__, $e->getMessage());

            $response = new ErrorResponse($request);
            $response->setErrorMessage($e->getMessage());

            if ($e->getCode() === CaletaErrorCode::TRANSACTION_NOT_FOUND){
                $response->setStatus(CaletaErrorCode::STATUS_RS_ERROR_TRANSACTION_DOES_NOT_EXIST);
            }else{
                $response->setStatus(CaletaErrorCode::STATUS_RS_ERROR_UNKNOWN);
            }

            return $response;
        }

        $walletResult = $this->_balance($this->_session);

        if (!$this->_validateWalletResult($walletResult)){
            $innerErrorCode = CaletaErrorHelper::getErrorCode($walletResult);
            $response = new ErrorResponse();
            $response->setMappedInnerErrorToProviderStatus($innerErrorCode);
            return $response;
        }

        $balance = $walletResult->getBalance();

        $response = new RollbackResponse($request);
        $response->setBalance($balance);

        return $this->_prepareResponse(
            $response,
            $request
        );
    }

    #endregion methods

    #region helpers

    /**
     * Метод выполняет финансовую транзакцию: bet, win, rollback
     * 1. Создаем казино транзакцию со статусом create
     * 2. Вызываем wallet
     * 3. Делаем обновление казино транзакции на статус processed/(error, wc_error, client_error)
     * 4. Если при обновлении статуса на processed возникла ошибка/исключение - нужно вернуть деньги в исходное состояние через wallet
     *
     * @param AbstractTransactionRequest $request
     */
    public function _executeTransactionRequest(AbstractTransactionRequest $request, string $extTrnType): CasinoGameTransactionModel
    {
        $provider = $this->getProvider();

        $transactionRepository = $this->getGameTransactionManager();
        $batchTransactionRepository = $this->getTransactionBatchManager();
        $moneyConverter = $this->getMoneyConverter();

        $session = $this->_getSessionByRequest($request);
        $sessionId = $session->getSessionId();
        $gameSessionRepository = $this->getGameManager();
        $gameSession = $gameSessionRepository->selectOneByExternalGameSessionId($sessionId);

        $gameSessionId = $gameSession->getId();
        $currency = $session->getCurrency();
        $userId = $session->getPlayerId();

        $extBetType = $extTrnType;
        $extGameRound = $request->getRoundId();
        $extTrnBatchId = $request->getRoundId();
        $extTrnId = $request->getTransactionId();
        $amountInCents = $request->getAmount();
        $extAmount = $moneyConverter->convert($amountInCents, $currency);

        $additionExtTrnData = '';

        switch ($extTrnType) {
            case CaletaConstants::TRANSACTION_TYPE_BET:
                $trnType = CasinoGameTransactionManagerInterface::TYPE_DEBIT;
                break;
            case CaletaConstants::TRANSACTION_TYPE_WIN:
                $trnType = CasinoGameTransactionManagerInterface::TYPE_CREDIT;
                break;
            case CaletaConstants::TRANSACTION_TYPE_ROLLBACK:
                $trnType = CasinoGameTransactionManagerInterface::TYPE_ROLLBACK;
                break;
            default:
                throw new LogicException(printf('Not supported external transaction type: type = %s', $extTrnType));
        }

        $transaction = $transactionRepository->insert(
            $gameSessionId,
            CasinoGameTransactionManagerInterface::STATUS_CREATED,
            $provider,
            $extTrnId,
            $trnType,
            $extTrnType,
            $amountInCents,
            $extAmount,
            $currency,
            0,
            $extTrnId,
            $extGameRound,
            $extBetType,
            $additionExtTrnData,
            null,
            0,
            null,
            0,
            $userId
        );

        if (empty($transaction)) {
            $this->addError('ERROR', __METHOD__, 'Insert transaction failed');
            throw new UnexpectedValueException('Insert transaction failed');
        }

        $batchTransaction = $batchTransactionRepository->insert($transaction->getId(), $extTrnBatchId);
        if (empty($batchTransaction)) {
            $this->addError('ERROR', __METHOD__, 'Insert batch transaction failed');
            throw new UnexpectedValueException('Insert batch transaction failed');
        }

        $refTransaction = null;

        if ($extTrnType === CaletaConstants::TRANSACTION_TYPE_ROLLBACK){
            $refTransaction = $this->_loadReferenceTransaction($request, $extTrnType);

            if (!$refTransaction){
                $this->_throwCasinoSdkCallbackException("Ref transaction not found", CaletaErrorCode::TRANSACTION_NOT_FOUND);
            }
        }

        return $this->_executeTransaction($transaction, $refTransaction);
    }

    protected function _loadReferenceTransaction(AbstractTransactionRequest $request, string $extTrnType): CasinoGameTransactionModel
    {
        $transactionRepository = $this->getGameTransactionManager();

        $casinoGameId = $this->_gameSession->getId();
        $roundId      = $request->getRoundId();
        $provider     = $this->getProvider();
        $userId       = $this->_session->getPlayerId();

        switch ($extTrnType){
            case CaletaConstants::TRANSACTION_TYPE_WIN:
                $transactionType = $transactionRepository::TYPE_CREDIT;
                break;
            case CaletaConstants::TRANSACTION_TYPE_ROLLBACK:
                $transactionType = null;
                break;
            default:
                $this->addError('ERROR', __METHOD__, 'UNDEFINED REFERENCE TRN TYPE');
                $this->_throwCasinoSdkCallbackException('UNDEFINED REFERENCE TRN TYPE');
        }

        $transactionId = $request->getReferenceTransactionId();

        $conditions = [
            'f_external_transaction_id'   => $transactionId,
            //'f_casino_player_game_id'     => $casinoGameId,
            //'f_external_game_round_id'    => $roundId,
            'f_provider'                  => $provider,
            'f_internal_player_id'        => $userId,
            //'f_status'                    => $transactionRepository::STATUS_PROCESSED,
        ];

        if ($transactionType){
            $conditions['f_transaction_type'] = $transactionType;
        }

        return $transactionRepository->selectOne($conditions);
    }

    protected function _executeTransaction(
        CasinoGameTransactionModel $currentTransaction,
        CasinoGameTransactionModel $currentRefTransaction = null
    ): CasinoGameTransactionModel
    {
        $transactionRepository = $this->getGameTransactionManager();

        $transaction = $currentTransaction;
        $refTransaction = $currentRefTransaction;

        if ($refTransaction) {
            $refExternalTransactionId = $refTransaction->getExternalTransactionId();
            $transaction->setExternalOriginalTransactionId($refExternalTransactionId);

            $refInternalTransactionId = $refTransaction->getInternalTransactionId();
            if ($refInternalTransactionId) {
                $transaction->setInternalRollbackTransactionId($refInternalTransactionId);
            }
        }

        $amountInCents = $transaction->getAmount();

        if ($currentTransaction->getTransactionType() === CasinoGameTransactionManagerInterface::TYPE_ROLLBACK){
            $amountInCents = $refTransaction->getAmount();
        }

        $executeWallet = !empty($amountInCents);

        try {
            if ($executeWallet) {
                $walletResult = $this->_executeWalletTransaction($transaction, $refTransaction);

                if (!$this->_validateWalletResult($walletResult)){
                    throw new CasinoWalletException($walletResult->getErrorDescription(), $walletResult->getErrorCode());
                }

                if ($walletResult instanceof DebitResult || $walletResult instanceof CreditResult) {
                    $internalTransactionId = $walletResult->getInternalTransactionId();
                    if (!empty($internalTransactionId)) {
                        $transaction->setInternalTransactionId($internalTransactionId);
                    }
                }
            }

            $transaction->setStatus(CasinoGameTransactionManagerInterface::STATUS_PROCESSED);

            if ($refTransaction) {
                $refTransaction->setStatus(CasinoGameTransactionManagerInterface::STATUS_ROLLEDBACK);
            }
        } catch (\Throwable $e) {

            if ($executeWallet && !isset($walletResult)){
                $this->addError('INNER ERROR', __METHOD__, $e->getMessage());
                $this->_throwCasinoSdkCallbackException($e->getMessage());
            }

            if ($executeWallet) {
                /** @var WalletResultAbstract $walletResult */
                $failWalletTrnData = $this->_extractFailWalletTransactionData($e, $walletResult);
            } else {
                $failWalletTrnData = [
                    $e->getCode(),
                    $e->getMessage() . " ( {$e->getCode()} )",
                    CasinoGameTransactionManagerInterface::ERROR_CODE_EXCEPTION,
                    $e->getMessage() . " ( {$e->getCode()} )",
                    CasinoGameTransactionManagerInterface::STATUS_ERROR,
                ];
            }

            /**
             * @var int    $errorCode
             * @var string $errorMessage
             * @var int    $trnErrorCode
             * @var string $trnErrorMessage
             * @var string $trnStatus
             */
            list($errorCode, $errorMessage, $trnErrorCode, $trnErrorMessage, $trnStatus) = $failWalletTrnData;

            $transaction->setErrorCode($trnErrorCode);
            $transaction->setErrorMessage($trnErrorMessage);
            $transaction->setStatus($trnStatus);

            $transactionRepository->update($transaction);
            if (!$transaction) {
                $this->addError('ERROR', __METHOD__, 'Update error transaction failed');
                throw new UnexpectedValueException('Update error transaction failed');
            }

            $this->addError('ERROR', __METHOD__, $errorMessage);

            throw $e;
        }

        try {
            $updateTransactionNum = 0;

            $transactionRepository->update($transaction);
            if (!$transaction) {
                $this->addError('ERROR', __METHOD__, 'Update transaction failed');
                throw new UnexpectedValueException('Update transaction failed');
            }

            if ($refTransaction) {
                $updateTransactionNum = 1;

                $transactionRepository->update($refTransaction);
                if (!$refTransaction) {
                    throw new UnexpectedValueException('Update ref transaction failed');
                }
            }
        } catch (\Throwable $e) {

            if ($executeWallet && $updateTransactionNum === 0) {
                $this->_rollbackWalletTransaction($transaction);
            }

            $errorCode = $e->getCode();

            $this->addError('ERROR', __METHOD__, $e->getMessage());
            $this->_throwCasinoSdkCallbackException($e->getMessage());
        }

        return $transaction;
    }

    protected function _rollbackWalletTransaction(CasinoGameTransactionModel $transaction)
    {
        $trnType = $transaction->getTransactionType();

        $rollbackTransaction = clone $transaction;

        if ($trnType === CasinoGameTransactionManagerInterface::TYPE_DEBIT) {
            $rollbackTransaction->setTransactionType(CasinoGameTransactionManagerInterface::TYPE_ROLLBACK);
        } elseif ($trnType === CasinoGameTransactionManagerInterface::TYPE_CREDIT) {
            $rollbackTransaction->setTransactionType(CasinoGameTransactionManagerInterface::TYPE_ROLLBACK_CREDIT);
        } else {
            $this->addError(
                'ERROR',
                __METHOD__,
                printf('Not supported transaction type for rollback after error: type = %s', $trnType)
            );

            return;
        }

        $refTransactionId         = $transaction->getExternalTransactionId();
        $refInternalTransactionId = $transaction->getInternalTransactionId();

        if (!empty($refInternalTransactionId)) {
            $rollbackTransaction->setInternalRollbackTransactionId($refInternalTransactionId);
        }
        $rollbackTransaction->setExternalOriginalTransactionId($refTransactionId);

        $rollbackTransaction->setExternalTransactionId($refTransactionId . '_rollback');
        $rollbackTransaction->setStatus(CasinoGameTransactionManagerInterface::STATUS_CREATED);
        $rollbackTransaction->setInternalTransactionId(0);
        $rollbackTransaction->setErrorMessage(null);
        $rollbackTransaction->setErrorCode(null);


        try {
            $walletResult = $this->_executeWalletTransaction($rollbackTransaction, $transaction);

            if (!$this->_validateWalletResult($walletResult)){
                throw new CasinoWalletException($walletResult->getErrorDescription(), $walletResult->getErrorCode());
            }

            if ($walletResult instanceof DebitResult || $walletResult instanceof CreditResult) {
                $internalTransactionId = $walletResult->getInternalTransactionId();
                if (!empty($internalTransactionId)) {
                    $transaction->setInternalTransactionId($internalTransactionId);
                }
            }
        } catch (\Throwable $e) {
            $this->addError(
                'ERROR',
                __METHOD__,
                var_export(
                    [
                        'code'            => $e->getCode(),
                        'message'         => $e->getMessage(),
                        'transactionData' => [
                            'provider'                      => $rollbackTransaction->getProvider(),
                            'casinoGameId'                  => $rollbackTransaction->getCasinoGameId(),
                            'externalTransactionId'         => $rollbackTransaction->getExternalTransactionId(),
                            'externalOriginalTransactionId' => $rollbackTransaction->getExternalOriginalTransactionId(),
                            'transactionType'               => $rollbackTransaction->getTransactionType(),
                            'amount'                        => $rollbackTransaction->getAmount(),
                            'externalAmount'                => $rollbackTransaction->getExternalAmount(),
                            'currency'                      => $rollbackTransaction->getCurrency(),
                            'internalTransactionId'         => $rollbackTransaction->getInternalTransactionId(),
                            'internalRollbackTransactionId' => $rollbackTransaction->getInternalRollbackTransactionId(),
                            'internalPlayerId'              => $rollbackTransaction->getInternalPlayerId(),
                        ],
                    ]
                )
            );

            return;
        }
    }

    protected function _extractFailWalletTransactionData(\Throwable $exception, $walletResult): array
    {
        $errorCode    = $exception->getCode();
        $errorMessage = $exception->getMessage();

        if ($walletResult instanceof WalletResultAbstract &&
            $walletResult->getErrorCode() !== WalletInterface::ERROR_CODE_COMPLETED_SUCCESSFULLY
        ) {
            $trnErrorCode    = $walletResult->getErrorCode();
            $trnErrorMessage = $walletResult->getErrorDescription();

            $session = $this->_session;

            try {
                if ($this->isClient($session)) {
                    $trnStatus = CasinoGameTransactionManagerInterface::STATUS_CLIENT_ERROR;
                } else {
                    $trnStatus = CasinoGameTransactionManagerInterface::STATUS_WC_ERROR;
                }
            } catch (Exception $e) {
                $trnStatus = CasinoGameTransactionManagerInterface::STATUS_ERROR;
            }
        } else {
            $trnErrorCode    = CasinoGameTransactionManagerInterface::ERROR_CODE_EXCEPTION;
            $trnErrorMessage = $exception->getMessage() . " ( $trnErrorCode )";
            $trnStatus       = CasinoGameTransactionManagerInterface::STATUS_ERROR;
        }

        return [
            $errorCode,
            $errorMessage,
            (int)$trnErrorCode,
            (string)$trnErrorMessage,
            $trnStatus,
        ];
    }

    protected function _validateWalletResult(WalletResultAbstract $walletResult): bool
    {
        if ($walletResult->getErrorCode() !== WalletInterface::ERROR_CODE_COMPLETED_SUCCESSFULLY) {
            $this->addError('ERROR', __METHOD__, $walletResult->getErrorDescription());
            return false;
        }

        return true;
    }

    protected function _executeWalletTransaction(
        CasinoGameTransactionModel $createdTransaction,
        CasinoGameTransactionModel $refTransaction = null
    ): WalletResultAbstract {
        $provider = $this->getProvider();

        $session     = $this->_session;
        $gameSession = $this->_gameSession;

        $trnType          = $createdTransaction->getTransactionType();
        $amountInCents    = $createdTransaction->getAmount();
        $extTrnType       = $createdTransaction->getExternalTransactionType();
        $extTrnId         = $createdTransaction->getExternalTransactionId();
        $extOriginalTrnId = $createdTransaction->getExternalOriginalTransactionId();
        $extGameRoundId   = $createdTransaction->getExternalGameRoundId();

        switch ($trnType) {
            case CasinoGameTransactionManagerInterface::TYPE_DEBIT:
                $walletResult = $this->_debit(
                    $session,
                    $gameSession,
                    $extTrnId,
                    $extTrnType,
                    $amountInCents,
                    $provider,
                    $extGameRoundId
                );

                break;
            case CasinoGameTransactionManagerInterface::TYPE_CREDIT:
                $walletResult = $this->_credit(
                    $session,
                    $gameSession,
                    $extTrnId,
                    $extTrnType,
                    $amountInCents,
                    $provider,
                    $extGameRoundId
                );

                break;
            case CasinoGameTransactionManagerInterface::TYPE_ROLLBACK:

                if (!($refTransaction instanceof CasinoGameTransactionModel)) {
                    $this->addError('ERROR', __METHOD__, "WRONG REF TRANSACTION TYPE");
                    throw new LogicException("WRONG REF TRANSACTION TYPE");
                }

                $amountInCents    = $refTransaction->getAmount();
                $extTrnId         = $refTransaction->getExternalTransactionId();
                $extOriginalTrnId = $refTransaction->getExternalOriginalTransactionId();
                $extGameRoundId   = $refTransaction->getExternalGameRoundId();


                if ($refTransaction->getTransactionType() === CasinoGameTransactionManagerInterface::TYPE_CREDIT){
                    $walletResult = $this->_rollbackCredit(
                        $session,
                        $gameSession,
                        $extTrnId,
                        $extOriginalTrnId,
                        $amountInCents,
                        $provider,
                        $refTransaction,
                        $extGameRoundId
                    );
                }elseif ($refTransaction->getTransactionType() === CasinoGameTransactionManagerInterface::TYPE_DEBIT){
                    $walletResult = $this->_rollbackDebit(
                        $session,
                        $gameSession,
                        $extTrnId,
                        $extOriginalTrnId,
                        $amountInCents,
                        $provider,
                        $refTransaction,
                        $extGameRoundId
                    );
                }else{
                    $this->addError('ERROR', __METHOD__, "Wrong rollback payment type");
                    throw new LogicException('Wrong rollback payment type');
                }

                break;
            default:
                throw new LogicException(printf('Not supported transaction type: type = %s', $trnType));
        }

        return $walletResult;
    }

    protected function _prepareSession(string $sessionToken)
    {
        $sessionRepository = $this->getSessionManager();
        $gameSessionRepository = $this->getGameManager();

        $session = $sessionRepository->selectOneBySessionId($sessionToken);

        if (!$session) {
            $this->addError(printf('Session not found: token = %s', $sessionToken), __METHOD__);
            return null;
        }

        $gameSession = $gameSessionRepository->selectOneByExternalGameSessionId($sessionToken);

        if (!$gameSession) {
            $this->addError(printf('Game session not found: token = %s', $sessionToken), __METHOD__);
            return null;
        }

        $session = $sessionRepository->updateById(
            $session->getId(),
            $session->getPlayerId(),
            $session->getSessionId(),
            $session->getProvider(),
            $session->getInternalGameId(),
            $session->getExternalGameId(),
            $session->getLocale(),
            $session->getCurrency(),
            $session->getExternalTableId()
        );

        if (!$session) {
            return null;
        }

        $gameSession = $gameSessionRepository->updateById(
            $gameSession->getId(),
            $gameSession->getCasinoSessionId(),
            $gameSession->getExternalGameSessionId(),
            $gameSession->getFinished()
        );

        if (!$gameSession) {
            return null;
        }

        return $session;
    }

    protected function _getSessionByRequest(AbstractRequest $request): CasinoSessionModel
    {
        $sessionId = $request->getSessionId() ?: $request->getSessionAlternativeId();

        return $this->getSessionManager()->selectOneBySessionId($sessionId);
    }

    protected function _prepareResponse(
        AbstractResponse   $response,
        AbstractRequest    $request,
        $status = CaletaErrorCode::STATUS_RS_OK
    )
    {
        $requestUuid = $request->getRequestUuid();

        $userId = $this->_session->getPlayerId();
        $currency = $this->_session->getCurrency();

        $response->setStatus($status);
        $response->setUser($userId);
        $response->setCurrency($currency);
        $response->setRequestUid($requestUuid);

        /** @var ErrorResponse $errorResponse */
        $this->_validateResponse($response, $request, $errorResponse);

        return $errorResponse ?? $response;
    }

    protected function _validateResponse(AbstractResponse $response, AbstractRequest $request, ErrorResponse &$errorResponse = null)
    {
        $response->validate();

        if ($response->hasErrors()) {
            $errorResponse = new ErrorResponse($request);
            $errorResponse->addErrors($response->getErrors());
            $errorResponse->setErrorCode(CaletaErrorCode::INTERNAL_ERROR);
        }
    }

    protected function _validateRequest(AbstractRequest $request, ErrorResponse &$response = null)
    {
        try{
            $request->validate();
            $this->_session = $this->_getSessionByRequest($request);
        }catch (\Throwable $e){
            $response = new ErrorResponse($request);
            $response->setErrorMessage($e->getMessage());
            $response->setMappedInnerErrorToProviderStatus($e->getCode());
            return;
        }

        if ($request->hasErrors()) {
            $requestUuid = $request->getRequestUuid();

            $firstError = $request->getFirstError();
            $errorCode = $firstError->getCode();

            if ($errorCode == CaletaErrorCode::ROLLBACK_TRANSACTION_NOT_FOUND) {
                $walletResult = $this->_balance($this->_session);

                if (!$this->_validateWalletResult($walletResult)){
                    $innerErrorCode = CaletaErrorHelper::getErrorCode($walletResult);
                    $response = new ErrorResponse();
                    $response->setMappedInnerErrorToProviderStatus($innerErrorCode);
                    return;
                }

                $balance = $walletResult->getBalance();
                $userId = $this->_session->getPlayerId();
                $currency = $this->_session->getCurrency();

                $response = new RollbackResponse($request);
                $response->setStatus(CaletaErrorCode::STATUS_RS_OK);
                $response->setBalance($balance);
                $response->setCurrency($currency);
                $response->setUser($userId);
                $response->setRequestUid($requestUuid);

                return;
            }

            $response = new ErrorResponse($request);
            $response->addErrors($request->getErrors());
            $response->setErrorCode(CaletaErrorCode::INTERNAL_ERROR);
            $response->setRequestUid($requestUuid);

            $response->setMappedInnerErrorToProviderStatus($errorCode);

            $this->addError('ERROR', __METHOD__, $request->getErrors(), '', 'Error validation request');

            return;
        }

        if (!$this->_session) {
            $response = new ErrorResponse($request);
            $response->setErrorCode(CaletaErrorCode::INTERNAL_ERROR);
            $response->setErrorMessage('Session not found');

            $this->addError('ERROR', __METHOD__, 'Session not found');

            return;
        }

        $sessionGameRepository = $this->getGameManager();
        $this->_gameSession = $sessionGameRepository->selectOneByExternalGameSessionId($this->_session->getSessionId());

        if (!$this->_gameSession) {
            $response = new ErrorResponse($request);
            $response->setErrorCode(CaletaErrorCode::INTERNAL_ERROR);
            $response->setErrorMessage('Game session not found, sessionId=' . $this->_session->getSessionId());

            $this->addError('ERROR', __METHOD__, 'Session not found');
        }
    }

    protected function _throwCasinoSdkCallbackException(string $message = '', int $code = CaletaErrorCode::INTERNAL_ERROR)
    {
        parent::_throwCasinoSdkCallbackException($message, $code);
    }

    protected function _getFullMessageException(\Throwable $e): string
    {
        return $e->getMessage() . '; file=' . $e->getFile() . '; line=' . $e->getLine();
    }

    #endregion helpers
}