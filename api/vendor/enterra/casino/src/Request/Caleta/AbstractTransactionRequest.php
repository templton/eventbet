<?php

namespace Enterra\Casino\Request\Caleta;

use Enterra\Casino\Constants\Caleta\CaletaConstants;

abstract class AbstractTransactionRequest extends AbstractRequest
{
    public function getAmount()
    {
        $value = $this->_data[CaletaConstants::REQUEST_AMOUNT] ?? 0;
        return $value/CaletaConstants::MONEY_MULTIPLE;
    }

    public function getTransactionId()
    {
        return $this->_data[CaletaConstants::REQUEST_TRANSACTION_UUID];
    }

    public function getReferenceTransactionId()
    {
        return $this->_data[CaletaConstants::REQUEST_REFERENCE_TRANSACTION_UUID];
    }

    public function getRefundedTransactionId()
    {
        return $this->_data[CaletaConstants::REQUEST_REFENDED_TRANSACTION_UUID];
    }

    public function getRoundId()
    {
        return $this->_data[CaletaConstants::REQUEST_ROUND];
    }

    public function getBetType()
    {
        return $this->_data[CaletaConstants::REQUEST_BET];
    }
}