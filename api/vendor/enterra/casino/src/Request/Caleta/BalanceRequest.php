<?php

namespace Enterra\Casino\Request\Caleta;

use Enterra\Casino\Common\Validators\RequiredValidator;
use Enterra\Casino\Common\Validators\Validator;
use Enterra\Casino\Common\Validators\ValidatorFactory;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use Enterra\Casino\ErrorCode\CaletaErrorCode;

class BalanceRequest extends AbstractRequest
{
    protected function _rules(): array
    {
        $parentRules = parent::_rules();

        $rules = [
            [
                ValidatorFactory::OPTION_CLASS => RequiredValidator::class,
                ValidatorFactory::OPTION_CONFIG => [
                    Validator::CONFIG_CODE => CaletaErrorCode::REQUEST_DATA_ERROR,
                    Validator::CONFIG_ATTRIBUTES => [
                        CaletaConstants::REQUEST_UUID,
                    ],
                ],
            ],
        ];

        return array_merge($parentRules, $rules);
    }
}