<?php

namespace Enterra\Casino\Request\Caleta;

use Enterra\Casino\Common\Validators\RequiredValidator;
use Enterra\Casino\Common\Validators\Transaction\AbstractTransactionValidator;
use Enterra\Casino\Common\Validators\Transaction\TransactionExistValidator;
use Enterra\Casino\Common\Validators\Transaction\TransactionNotExistValidator;
use Enterra\Casino\Common\Validators\Validator;
use Enterra\Casino\Common\Validators\ValidatorFactory;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use Enterra\Casino\ErrorCode\CaletaErrorCode;
use Enterra\Casino\Repository\GameTransaction\CasinoGameTransactionManagerInterface;
use Enterra\Casino\Repository\GameTransaction\CasinoGameTransactionManagerYii;

class RollbackRequest extends AbstractTransactionRequest
{
    protected function _rules(): array
    {
        $parentRules = parent::_rules();

        $rules = [
            [
                ValidatorFactory::OPTION_CLASS => RequiredValidator::class,
                ValidatorFactory::OPTION_CONFIG => [
                    Validator::CONFIG_CODE => CaletaErrorCode::REQUEST_DATA_ERROR,
                    Validator::CONFIG_ATTRIBUTES => [
                        CaletaConstants::REQUEST_TRANSACTION_UUID,
                        CaletaConstants::REQUEST_REFERENCE_TRANSACTION_UUID,
                        CaletaConstants::REQUEST_UUID,
                        CaletaConstants::REQUEST_GAME_ID,
                        CaletaConstants::REQUEST_GAME_CODE,
                    ],
                ],
            ],
            [
                ValidatorFactory::OPTION_CLASS      => TransactionNotExistValidator::class,
                ValidatorFactory::OPTION_CONFIG     => [
                    Validator::CONFIG_CODE                      => CaletaErrorCode::ROLLBACK_ALREADY_PROCESSED,
                    Validator::CONFIG_MESSAGE                   => CaletaErrorCode::ROLLBACK_ALREADY_PROCESSED_MESSAGE,
                    TransactionExistValidator::CONFIG_BY        => TransactionNotExistValidator::BY_EXTERNAL_TRANSACTION_ID,
                    TransactionExistValidator::CONFIG_PROVIDER  => $this->getProvider(),
                    Validator::CONFIG_ATTRIBUTES                => [
                        CaletaConstants::REQUEST_TRANSACTION_UUID,
                    ],
                ],
                ValidatorFactory::OPTION_DEPENDENCE => [
                    AbstractTransactionValidator::DEPENDENCE_TRANSACTION_REPOSITORY => CasinoGameTransactionManagerYii::class,
                ],
            ],
            [
                ValidatorFactory::OPTION_CLASS      => TransactionExistValidator::class,
                ValidatorFactory::OPTION_CONFIG     => [
                    Validator::CONFIG_CODE                      => CaletaErrorCode::ROLLBACK_TRANSACTION_NOT_FOUND,
                    Validator::CONFIG_MESSAGE                   => CaletaErrorCode::ROLLBACK_TRANSACTION_NOT_FOUND_MESSAGE,
                    TransactionExistValidator::CONFIG_BY        => TransactionNotExistValidator::BY_EXTERNAL_TRANSACTION_ID,
                    TransactionExistValidator::CONFIG_PROVIDER  => $this->getProvider(),
                    TransactionExistValidator::CONFIG_STATUS    => CasinoGameTransactionManagerInterface::STATUS_PROCESSED,
                    Validator::CONFIG_ATTRIBUTES                => [
                        CaletaConstants::REQUEST_REFERENCE_TRANSACTION_UUID,
                    ],
                ],
                ValidatorFactory::OPTION_DEPENDENCE => [
                    AbstractTransactionValidator::DEPENDENCE_TRANSACTION_REPOSITORY => CasinoGameTransactionManagerYii::class,
                ],
            ],
        ];

        return array_merge($parentRules, $rules);
    }
}