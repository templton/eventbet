<?php

namespace Enterra\Casino\Request\Caleta;

use Enterra\Casino\Common\Validators\ClosureValidator;
use Enterra\Casino\Common\Validators\RequiredValidator;
use Enterra\Casino\Common\Validators\Session\AbstractSessionValidator;
use Enterra\Casino\Common\Validators\Session\SessionExistValidator;
use Enterra\Casino\Common\Validators\Session\SessionExpiredValidator;
use Enterra\Casino\Common\Validators\Validator;
use Enterra\Casino\Common\Validators\ValidatorFactory;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use Enterra\Casino\ErrorCode\CaletaErrorCode;
use Enterra\Casino\Helpers\TypecastHelper;
use Enterra\Casino\Repository\Session\CasinoSessionManagerYii;
use Enterra\Casino\Request\Caleta\Helpers\SignHelper;
use Enterra\Casino\Request\AbstractRequest as AbstractRequestCommon;

class AbstractRequest extends AbstractRequestCommon
{
    protected $rawBody;

    protected $sign;

    public function getRequestUuid()
    {
        return $this->getValue(CaletaConstants::REQUEST_UUID);
    }

    public function getSessionId()
    {
        return $this->getValue(CaletaConstants::REQUEST_TOKEN);
    }

    public function setRawBody($rawBody)
    {
        $this->rawBody = $rawBody;
    }

    public function getRawBody()
    {
        return $this->rawBody;
    }

    public function setSign($sign)
    {
        $this->sign = $sign;
    }

    public function getSign()
    {
        return $this->sign;
    }

    protected function _dataTypes(): array
    {
        $parentDataTypes = parent::_dataTypes();

        $dataTypes = [
            CaletaConstants::REQUEST_HEADER_SIGN => TypecastHelper::TYPE_STRING,
            CaletaConstants::REQUEST_TOKEN => TypecastHelper::TYPE_STRING,
            CaletaConstants::REQUEST_UUID => TypecastHelper::TYPE_STRING,
            CaletaConstants::REQUEST_TRANSACTION_UUID => TypecastHelper::TYPE_STRING,
            CaletaConstants::REQUEST_REFERENCE_TRANSACTION_UUID => TypecastHelper::TYPE_STRING,
            CaletaConstants::REQUEST_ROUND => TypecastHelper::TYPE_STRING,
            CaletaConstants::REQUEST_GAME_CODE => TypecastHelper::TYPE_STRING,
            CaletaConstants::REQUEST_CURRENCY => TypecastHelper::TYPE_STRING,
            CaletaConstants::REQUEST_AMOUNT => TypecastHelper::TYPE_INTEGER,
        ];

        return array_merge($parentDataTypes, $dataTypes);
    }

    protected function _rules(): array
    {
        $parentRules = parent::_rules();

        $rules = [
            [
                ValidatorFactory::OPTION_CLASS  => RequiredValidator::class,
                ValidatorFactory::OPTION_CONFIG => [
                    Validator::CONFIG_CODE       => CaletaErrorCode::REQUEST_DATA_ERROR,
                    Validator::CONFIG_ATTRIBUTES => [
                        CaletaConstants::REQUEST_TOKEN
                    ],
                ],
            ],
            [
                ValidatorFactory::OPTION_CLASS => SessionExistValidator::class,
                ValidatorFactory::OPTION_CONFIG => [
                    Validator::CONFIG_CODE => CaletaErrorCode::SESSION_NOT_EXISTS,
                    Validator::CONFIG_MESSAGE => CaletaErrorCode::SESSION_NOT_EXISTS_MESSAGE,
                    SessionExistValidator::CONFIG_BY       => SessionExistValidator::BY_SESSION_ID,
                    Validator::CONFIG_ATTRIBUTES           => [
                        CaletaConstants::REQUEST_TOKEN,
                    ],
                ],
                ValidatorFactory::OPTION_DEPENDENCE => [
                    AbstractSessionValidator::DEPENDENCE_SESSION_REPOSITORY => CasinoSessionManagerYii::class,
                ],
            ],
            [
                ValidatorFactory::OPTION_CLASS      => SessionExpiredValidator::class,
                ValidatorFactory::OPTION_CONFIG     => [
                    Validator::CONFIG_CODE                 => CaletaErrorCode::TOKEN_IS_OUT_OF_DATE,
                    Validator::CONFIG_MESSAGE              => CaletaErrorCode::TOKEN_IS_OUT_OF_DATE_MESSAGE,
                    SessionExistValidator::CONFIG_BY       => SessionExistValidator::BY_SESSION_ID,
                    SessionExistValidator::CONFIG_PROVIDER => $this->getProvider(),
                    Validator::CONFIG_ATTRIBUTES           => [
                        CaletaConstants::REQUEST_TOKEN,
                    ],
                ],
                ValidatorFactory::OPTION_DEPENDENCE => [
                    AbstractSessionValidator::DEPENDENCE_SESSION_REPOSITORY => CasinoSessionManagerYii::class,
                ],
            ],
            [
                ValidatorFactory::OPTION_CLASS  => ClosureValidator::class,
                ValidatorFactory::OPTION_CONFIG => [
                    Validator::CONFIG_CODE           => CaletaErrorCode::SIGNATURE_INVALID,
                    Validator::CONFIG_SKIP_ON_EMPTY  => false,
                    ClosureValidator::CONFIG_CLOSURE => function () {
                        $sign    = $this->getSign();
                        $rawBody = $this->getRawBody();

                        return SignHelper::validateSign($sign, $rawBody);
                    },
                    Validator::CONFIG_ATTRIBUTES     => [
                        CaletaConstants::REQUEST_TOKEN
                    ],
                ],
            ],
        ];

        return array_merge($parentRules, $rules);
    }

}