<?php

namespace Enterra\Casino\Request\Caleta;

use Enterra\Casino\Common\Validators\CompareValidator;
use Enterra\Casino\Common\Validators\RequiredValidator;
use Enterra\Casino\Common\Validators\Transaction\AbstractTransactionValidator;
use Enterra\Casino\Common\Validators\Transaction\TransactionExistValidator;
use Enterra\Casino\Common\Validators\Transaction\TransactionNotExistValidator;
use Enterra\Casino\Common\Validators\Validator;
use Enterra\Casino\Common\Validators\ValidatorFactory;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use Enterra\Casino\ErrorCode\CaletaErrorCode;
use Enterra\Casino\Repository\GameTransaction\CasinoGameTransactionManagerInterface;
use Enterra\Casino\Repository\GameTransaction\CasinoGameTransactionManagerYii;

class BetRequest extends AbstractTransactionRequest
{
    protected function _rules(): array
    {
        $parentRules = parent::_rules();

        $rules = [
            [
                ValidatorFactory::OPTION_CLASS => RequiredValidator::class,
                ValidatorFactory::OPTION_CONFIG => [
                    Validator::CONFIG_CODE => CaletaErrorCode::REQUEST_DATA_ERROR,
                    Validator::CONFIG_ATTRIBUTES => [
                        CaletaConstants::REQUEST_AMOUNT,
                        CaletaConstants::REQUEST_GAME_CODE,
                        CaletaConstants::REQUEST_GAME_ID,
                        CaletaConstants::REQUEST_ROUND,
                        CaletaConstants::REQUEST_TRANSACTION_UUID,
                    ],
                ],
            ],
            [
                ValidatorFactory::OPTION_CLASS => CompareValidator::class,
                ValidatorFactory::OPTION_CONFIG => [
                    Validator::CONFIG_CODE => CaletaErrorCode::AMOUNT_IS_WRONG,
                    Validator::CONFIG_ATTRIBUTES => [
                        CaletaConstants::REQUEST_AMOUNT,
                    ],
                    CompareValidator::CONFIG_TYPE => CompareValidator::TYPE_NUMBER,
                    CompareValidator::CONFIG_OPERATOR => '>',
                    CompareValidator::CONFIG_COMPARE_VALUE => '0',
                ],
            ],
            [
                ValidatorFactory::OPTION_CLASS      => TransactionNotExistValidator::class,
                ValidatorFactory::OPTION_CONFIG     => [
                    Validator::CONFIG_CODE                                      => CaletaErrorCode::BET_ALREADY_PROCESSED,
                    TransactionExistValidator::CONFIG_BY                        => TransactionNotExistValidator::BY_EXTERNAL_TRANSACTION_ID,
                    TransactionExistValidator::CONFIG_PROVIDER                  => $this->getProvider(),
                    TransactionExistValidator::CONFIG_TYPE                      => CasinoGameTransactionManagerInterface::TYPE_DEBIT,
                    Validator::CONFIG_ATTRIBUTES                                => [
                        CaletaConstants::REQUEST_TRANSACTION_UUID,
                    ],
                ],
                ValidatorFactory::OPTION_DEPENDENCE => [
                    AbstractTransactionValidator::DEPENDENCE_TRANSACTION_REPOSITORY => CasinoGameTransactionManagerYii::class,
                ],
            ],
        ];

        return array_merge($parentRules, $rules);
    }
}