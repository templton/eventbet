<?php

namespace Enterra\Casino\Request\Caleta\Helpers;

use Enterra\Casino\CallbackSdk\CasinoSdkCallbackAbstract;
use Enterra\Casino\CallbackSdk\CasinoSdkCallbackCaleta;
use Enterra\Casino\CasinoServiceManager;
use Enterra\Casino\Constants\Caleta\CaletaConstants;

class SignHelper
{
    public static function validateSign($signEncoded, $requestStr)
    {
        $sign = base64_decode($signEncoded);
        $key = self::_getPublicKey();

        $resultVerify = openssl_verify(
            $requestStr,
            $sign,
            $key,
            OPENSSL_ALGO_SHA256
        );

        openssl_free_key($key);

        return $resultVerify === 1;
    }

    protected static function _getPublicKey()
    {
        $container = CasinoServiceManager::getInstance();
        /** @var CasinoSdkCallbackCaleta $callbackService */
        $callbackService = $container->get(CasinoSdkCallbackAbstract::class . ucfirst(CaletaConstants::PROVIDER_NAME));
        $configs = $callbackService->getConfig();

        $fileKey = $configs[CaletaConstants::CONFIG_FIELD_PUBLIC_KEY] ?? '';
        $fileKey = preg_replace('@(?<!(GIN|RSA|ATE|KEY|END|LIC))\s@', "\n", $fileKey);

        return openssl_pkey_get_public($fileKey);
    }
}