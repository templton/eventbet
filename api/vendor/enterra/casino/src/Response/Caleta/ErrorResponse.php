<?php

namespace Enterra\Casino\Response\Caleta;

use Enterra\Casino\Common\Exceptions\CasinoWalletException;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use Enterra\Casino\ErrorCode\CaletaErrorCode;
use Enterra\Casino\Request\Caleta\AbstractRequest;
use Enterra\Casino\Response\ResponseErrorInterface;

class ErrorResponse extends AbstractResponse implements ResponseErrorInterface
{
    public function setRequestUid(string $requestUid)
    {
        $this->setValue(CaletaConstants::REQUEST_UUID, $requestUid);
    }

    public static function createFromAnyException(AbstractRequest $request, $e)
    {
        switch (get_class($e)){
            case CasinoWalletException::class:
                $response = self::createFromCasinoWalletException($request, $e);
                break;
            default:
                $response = new self($request);
                $response->setErrorMessage($e->getMessage());
                $response->setStatus(CaletaErrorCode::STATUS_RS_ERROR_UNKNOWN);
        }

        return $response;
    }

    public static function createFromCasinoWalletException(AbstractRequest $request, CasinoWalletException $e): self
    {
        $walletErrorCode = $e->getCode();

        $response = new self($request);
        $response->setErrorMessage($e->getMessage());
        $response->setStatus(self::getCaletaStatusByWalletError($walletErrorCode));

        return $response;
    }

    public static function createSessionNotFound(AbstractRequest $request)
    {
        $response = new self($request);
        $response->setStatus(CaletaErrorCode::STATUS_RS_ERROR_INVALID_TOKEN);
        $response->setErrorMessage('Session not found, sessionId=' . $request->getSessionId());

        return $response;
    }

    public static function getCaletaStatusByWalletError(int $walletErrorCode)
    {
        switch ($walletErrorCode){
            case CaletaErrorCode::WALLET_ERROR_CODE_NOT_ENOUGH_MONEY:
                return CaletaErrorCode::STATUS_RS_ERROR_NOT_ENOUGH_MONEY;
                break;
            default:
                return CaletaErrorCode::STATUS_RS_ERROR_UNKNOWN;
        }
    }

    /**
     * @param string $code
     */
    public function setErrorCode(string $code)
    {
        $this->errorCode = $code;
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    public function setErrorMessage(string $message)
    {
        $this->setValue('message', $message);
    }

    public function setMappedInnerErrorToProviderStatus(int $innerError)
    {
        switch ($innerError){
            case CaletaErrorCode::REQUEST_DATA_ERROR:
                $status = CaletaErrorCode::STATUS_RS_ERROR_UNKNOWN;
                break;
            case CaletaErrorCode::SESSION_NOT_EXISTS:
                $status = CaletaErrorCode::STATUS_RS_ERROR_INVALID_TOKEN;
                break;
            case CaletaErrorCode::TOKEN_IS_OUT_OF_DATE:
                $status = CaletaErrorCode::STATUS_RS_ERROR_TOKEN_EXPIRED;
                break;
            case CaletaErrorCode::BET_ALREADY_PROCESSED:
            case CaletaErrorCode::WIN_ALREADY_PROCESSED:
            case CaletaErrorCode::ROLLBACK_ALREADY_PROCESSED:
                $status = CaletaErrorCode::STATUS_RS_ERROR_DUPLICATE_TRANSACTION;
                break;
            case CaletaErrorCode::SIGNATURE_INVALID:
                $status = CaletaErrorCode::STATUS_RS_ERROR_INVALID_SIGNATURE;
                break;
            case CaletaErrorCode::NOT_ENOUGH_MONEY:
                $status = CaletaErrorCode::STATUS_RS_ERROR_NOT_ENOUGH_MONEY;
                break;
            case CaletaErrorCode::ROLLBACK_TRANSACTION_NOT_FOUND:
                $status = CaletaErrorCode::STATUS_RS_OK;
                break;
            default:
                $status = CaletaErrorCode::STATUS_RS_ERROR_UNKNOWN;
        }

        $this->setStatus($status);
    }

}