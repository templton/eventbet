<?php

namespace Enterra\Casino\Response\Caleta;

use Enterra\Casino\Common\Validators\RequiredValidator;
use Enterra\Casino\Common\Validators\ValidatorFactory;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use Enterra\Casino\ErrorCode\CaletaErrorCode;
use Enterra\Casino\Helpers\TypecastHelper;
use Enterra\Casino\Response\AbstractResponse as AbstractResponseCommon;

class AbstractResponse extends AbstractResponseCommon
{
    public function setUser(string $userName)
    {
        $this->setValue(CaletaConstants::RESPONSE_USER, $userName);
    }

    public function setStatus(string $status)
    {
        $this->setValue(CaletaConstants::RESPONSE_STATUS, $status);
    }

    public function getStatus()
    {
        return $this->getValue(CaletaConstants::RESPONSE_STATUS);
    }

    public function setCurrency(string $currency)
    {
        $this->setValue(CaletaConstants::RESPONSE_CURRENCY, $currency);
    }

    public function setBalance($balance)
    {
        $balance *= CaletaConstants::MONEY_MULTIPLE;

        $this->setValue(CaletaConstants::RESPONSE_BALANCE, $balance);
    }

    public function _dataTypes(): array
    {
        $parentDataTypes = parent::_dataTypes();

        $dataTypes = [
            CaletaConstants::RESPONSE_USER => TypecastHelper::TYPE_STRING,
            CaletaConstants::RESPONSE_STATUS => TypecastHelper::TYPE_STRING,
            CaletaConstants::RESPONSE_CURRENCY => TypecastHelper::TYPE_STRING,
            CaletaConstants::RESPONSE_BALANCE => TypecastHelper::TYPE_FLOAT,
        ];

        return array_merge($parentDataTypes, $dataTypes);
    }

    public function _rules(): array
    {
        $parentRules = parent::_rules();

        $rules = [
            [
                ValidatorFactory::OPTION_CLASS  => RequiredValidator::class,
                ValidatorFactory::OPTION_CONFIG => [
                    RequiredValidator::CONFIG_CODE       => CaletaErrorCode::INTERNAL_ERROR,
                    RequiredValidator::CONFIG_ATTRIBUTES => [
                        CaletaConstants::RESPONSE_USER,
                        CaletaConstants::RESPONSE_STATUS,
                        CaletaConstants::RESPONSE_CURRENCY,
                    ],
                ],
            ],
        ];

        return array_merge($parentRules, $rules);
    }

    public function _beforeSerialize()
    {
        $needIncludeDetailError = $this->getStatus() !== CaletaErrorCode::STATUS_RS_ERROR_INVALID_SIGNATURE;

        if ($this->hasErrors() && $needIncludeDetailError) {
            $errors = [];
            foreach ($this->getErrors() as $key => $error) {
                $errors[$key] = $error[0]->getMessage();
            }
            $this->setValue('errors', $errors);
        }

        parent::_beforeSerialize();
    }

}