<?php

namespace Enterra\Casino\Response\Caleta;

use Enterra\Casino\Common\Validators\RequiredValidator;
use Enterra\Casino\Common\Validators\ValidatorFactory;
use Enterra\Casino\Constants\Caleta\CaletaConstants;
use Enterra\Casino\ErrorCode\CaletaErrorCode;

class BalanceResponse extends AbstractResponse
{
    public function setRequestUid(string $requestUid)
    {
        $this->setValue(CaletaConstants::REQUEST_UUID, $requestUid);
    }

    public function _rules(): array
    {
        $parentRules = parent::_rules();

        $rules = [
            [
                ValidatorFactory::OPTION_CLASS  => RequiredValidator::class,
                ValidatorFactory::OPTION_CONFIG => [
                    RequiredValidator::CONFIG_CODE       => CaletaErrorCode::INTERNAL_ERROR,
                    RequiredValidator::CONFIG_ATTRIBUTES => [
                        CaletaConstants::REQUEST_UUID,
                    ],
                ],
            ],
        ];

        return array_merge($parentRules, $rules);
    }
}