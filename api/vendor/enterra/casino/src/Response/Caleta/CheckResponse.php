<?php

namespace Enterra\Casino\Response\Caleta;

use Enterra\Casino\Constants\Caleta\CaletaConstants;

class CheckResponse extends AbstractResponse
{
    public function setToken(string $token)
    {
        $this->setValue(CaletaConstants::REQUEST_TOKEN, $token);
    }
}