<?php

namespace Enterra\Casino\ErrorCode;

class CaletaErrorCode
{
    const INTERNAL_ERROR                    = 0;
    const SIGN_NOT_PROVIDED_CODE            = 1;
    const SIGN_WRANG_CODE                   = 2;
    const TRANSACTION_NOT_FOUND             = 3;
    const REF_TRANSACTION_NOT_FOUND         = 4;
    const ROLLBACK_TRANSACTION_NOT_FOUND    = 5;
    const REQUEST_DATA_ERROR                = 6;
    const SESSION_NOT_EXISTS                = 7;
    const TOKEN_IS_OUT_OF_DATE              = 8;
    const AMOUNT_IS_WRONG                   = 9;
    const BET_ALREADY_PROCESSED             = 10;
    const WIN_ALREADY_PROCESSED             = 11;
    const ROLLBACK_ALREADY_PROCESSED        = 12;
    const SIGNATURE_INVALID                 = 13;
    const USER_IS_NOT_FOUND                 = 14;
    const NOT_ENOUGH_MONEY                  = 15;
    const SYSTEM_ERROR                      = 16;
    const WALLET_ERROR_CODE_NOT_ENOUGH_MONEY= 502;

    const SESSION_NOT_EXISTS_MESSAGE                = 'Session not exists';
    const TOKEN_IS_OUT_OF_DATE_MESSAGE              = 'Token is out of date';
    const SIGN_NOT_PROVIDED_TEXT                    = 'Sign or auth header were not provided';
    const SIGN_WRANG_TEXT                           = 'Sign is wrong';
    const AMOUNT_IS_WRONG_TEXT                      = 'Amount value is wrong';
    const BET_ALREADY_PROCESSED_MESSAGE             = 'Bet already processed';
    const ROLLBACK_TRANSACTION_NOT_FOUND_MESSAGE    = 'Reference transaction not found or already processed';
    const ROLLBACK_ALREADY_PROCESSED_MESSAGE        = 'Rollback already processed';

    //when transaction was processed correctly or was already processed before
    const STATUS_RS_OK = 'RS_OK';
    //use this when you need a generic error code
    const STATUS_RS_ERROR_UNKNOWN = 'RS_ERROR_UNKNOWN';
    //when the server was disconnected by timeout
    const STATUS_RS_ERROR_TIMEOUT = 'RS_ERROR_TIMEOUT';
    //when token never existed or is not associated to this player
    const STATUS_RS_ERROR_INVALID_TOKEN = 'RS_ERROR_INVALID_TOKEN';
    //when the game doesn't exist
    const STATUS_RS_ERROR_INVALID_GAME = 'RS_ERROR_INVALID_GAME';
    //when the currency is not active or does not exist
    const STATUS_RS_ERROR_WRONG_CURRENCY = 'RS_ERROR_WRONG_CURRENCY';
    //when user is short of money
    const STATUS_RS_ERROR_NOT_ENOUGH_MONEY = 'RS_ERROR_NOT_ENOUGH_MONEY';
    //when user is banned or inactive
    const STATUS_RS_ERROR_USER_DISABLED = 'RS_ERROR_USER_DISABLED';
    //when signature doesn't match
    const STATUS_RS_ERROR_INVALID_SIGNATURE = 'RS_ERROR_INVALID_SIGNATURE';
    //when a new token exists (this rule applies only for /wallet/bet)
    const STATUS_RS_ERROR_TOKEN_EXPIRED = 'RS_ERROR_TOKEN_EXPIRED';
    //message can't be decoded to JSON
    const STATUS_RS_ERROR_WRONG_SYNTAX = 'RS_ERROR_WRONG_SYNTAX';
    //i.e. when a type should be integer and came as string
    const STATUS_RS_ERROR_WRONG_TYPES = 'RS_ERROR_WRONG_TYPES';
    //when the same transaction_uuid appears but for a different user, round and game, otherwise returns RS_OK
    const STATUS_RS_ERROR_DUPLICATE_TRANSACTION = 'RS_ERROR_DUPLICATE_TRANSACTION';
    //when doing a 'win' transaction and the 'bet' transaction id doesn't exist
    const STATUS_RS_ERROR_TRANSACTION_DOES_NOT_EXIST = 'RS_ERROR_TRANSACTION_DOES_NOT_EXIST';
    //when a 'win' transaction happens in name of a rolled back transaction (when receiving a 'rollback' replies with 'RS_OK'
    const STATUS_RS_ERROR_TRANSACTION_ROLLED_BACK = 'RS_ERROR_TRANSACTION_ROLLED_BACK';
    //when the user exceeded its limit
    const STATUS_RS_ERROR_BET_LIMIT_EXCEEDED = 'RS_ERROR_BET_LIMIT_EXCEEDED';
}